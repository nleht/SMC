/*
 * File: wait.h
 * ------------
 */

#ifndef _wait_h_
#define _wait_h_

#include "util.h"

void InitWait(unsigned long cycle,unsigned long linelen);
void ResetWait(void);
bool Wait(void);

#endif
