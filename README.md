# Stanford Monte Carlo

Monte Carlo Model of energetic electrons and photons. It is used to model relativistic runaway electron avalanche (RREA), energetic electron precipitation (EEP), terrestrial gamma flashes (TGF) etc.

## License
The project is free to use, but is still protected by copyright. If you use it in a scientific publication, cite the following paper:

- Lehtinen, N. G., T. F. Bell and U. S. Inan (1999), Monte Carlo Simulation of Runaway MeV Electron Breakdown with Application to Red Sprites and Terrestrial Gamma Ray Flashes, _J. Geophys. Res._, **104**(A11), 24699-24712, November 1, doi:10.1029/1999JA900335.

