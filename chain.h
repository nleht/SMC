/*
 * File: chain.h
 * -------------
 */

#ifndef _chain_h_
#define _chain_h_

/* Incomplete type */
typedef struct ChainCDT *Chain;

/* Constructor */
Chain NewChain(const char *str);
const char *ChainString(Chain c);
void AppendToChain(Chain c,const char *suf);
/* Cut off the recently appended suffix */
void RestoreChain(Chain c);
void AppendRestoreChain(Chain c,const char *suf,bool append);
/* Destructor */
void DestroyChain(Chain c);

#endif /* _chain_h_ */
