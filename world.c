/*
 * File: world.c
 * -------------
 * Contains data on atmosphere density, electric field, magnetic field
 * and other stuff.
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "interface.h"
#include "world.h"

/* recommended SIMPLE_DEPOSIT_ENERGY=0 */
#define SIMPLE_DEPOSIT_ENERGY   0

/* Old version (soon to be changed) - density and field geometry same */
typedef enum {UNIFORM,LAYERED,CARTESIAN,CYLINDRICAL} GeometryT;
/* Magnetic field is always uniform */

/* Static function prototypes */
static void InitNonuniformWorld(World w);
static void InitNonuniformB(World w);
static void InitNonuniformEfield(World w);
#if 0
static void InitNm1(World w); /* not used */
#endif
static void InitNm(World w);
static void InitTAC(World w);

/* The structure type */
struct WorldCDT {
	GeometryT type;
	double BxUniform,ByUniform,BzUniform,BabsUniform; /* only uniform B */
	/* Electric field data */
	bool useE; /* Is there electric field at all? */
	double ExUniform,EzUniform,EabsUniform;
	double *E1D; /* array for electric field for 1D model, only vertical */
	double **Ex,**Ez; /* arrays for the electric field */
	double xEmax,xEmin,zEmin,zEmax;/* physical dimensions for E along (x,z) or (r,z), in km */
	int nxE,nzE; /* E field array dimensions along (x,z) or (r,z) */
	double dxE,dzE; /* grid sizes in km */
	/* Atmosphere density data */
	double *Nm; /* array for atmosphere density */
	double *TAC; /* array for total atmospheric contents */
	double zNmmax; /* zNmmin==0 */
	int nzNm;
	double dzNm;
	/* Auxiliary */
	double scaleHeight;
	/* Deposited energy as a function of altitude
	   - used only for a non-uniform world */
	double *endep;
	/* For a nonuniform simulation with different charges */
	double qQEH,Escale;
};

/************ MEMBER FUNCTION DEFINITIONS **************/

World InitWorld(void)
{
	World w;
	int type;

	if(!(w=malloc(sizeof(struct WorldCDT))))
		Error("InitWorld: Not enough memory");
	GetNumbers("Geometry: 1-uniform,2-one-dim,3-cartesian,4-cylindrical","d",&type);
	switch(type){
	case 1:
		w->type=UNIFORM;
		break;
	case 2:
		w->type=LAYERED;
		break;
	case 3:
		w->type=CARTESIAN;
		break;
	case 4:
		w->type=CYLINDRICAL;
		break;
	default:
		Error("Bad density geometry");
		break;
	}
	if(w->type!=UNIFORM){
		Printf("Initializing non-uniform world\n");
		InitNonuniformWorld(w);
	}else{
		Printf("Initializing uniform world\n");
		w->Ex=w->Ez=NULL;
		w->E1D=NULL;
		w->Nm=NULL;
	}
	return w;
}

static void InitNonuniformWorld(World w)
{
	InitNonuniformB(w);
	GetNumbers("Use electric field (y/n)?","b",&(w->useE));
	if(w->useE)	InitNonuniformEfield(w);
	InitNm(w);
	InitTAC(w);
	GetNumbers("Scale height for electron step (km):","f",&(w->scaleHeight));
}

static void InitNonuniformB(World w)
{
	double Bx,By,Bz,scale;

	GetNumbers("Magnetic field components (Tesla)?","fff",&Bx,&By,&Bz);
	/* Convert to the field in inverse seconds */
	scale=1.75882e11; /* =e/m */
	/*if(w->type==CYLINDRICAL){
		if(Bx>1e-8 || By>1e-8)
			Printf("The B field has to be vertical in cylindrical model");
		Bx=0; By=0;
	}*/
	w->BxUniform=Bx*scale;
	w->ByUniform=By*scale;
	w->BzUniform=Bz*scale;
	w->BabsUniform=sqrt(Bx*Bx+By*By+Bz*Bz)*scale;
}

#if 0
/* Not used */
static void InitNm1(World w)
{
	char Nmfname[80];
	GetNumbers("Density data file name:","s",80,Nmfname);
	GetNumbers("Density vertical dimension (km):","f",&(w->zNmmax));
	w->Nm=ReadArray(Nmfname,&(w->nzNm));
	/* Convert to dimensionless [skipped] */
	w->dzNm=w->zNmmax/(w->nzNm - 1);
	Printf("dz for Nm = %g\n",w->dzNm);
}
#endif

/*
 * InitNm
 * ------
 * Load Nm from a file, and convert it to dimensionless.
 */
static void InitNm(World w)
{
	double **nm;
	int dumb,i;
	char Nmfname[80];

	GetNumbers("Density data file name:","s",80,Nmfname);
	nm=ReadMatrix(Nmfname,&(w->nzNm),&dumb);
	if(dumb!=2) Error("file \"%s\" is bad",Nmfname);
	w->Nm=Allocate1(w->nzNm);
	for(i=0; i<w->nzNm; i++){
		/* To our units;
		 *  NmConv=1/(2*pi*rclass0^2*c*Zm)/(1 sec) = 4.6e18 m^{-3}
		 *  Zm is the average number of electrons in a molecule = 14.5 */
		w->Nm[i]=nm[i][1]/4.6e18;
	}
	w->dzNm=(nm[1][0]-nm[0][0])/1e3; /* in km */
	w->zNmmax=(nm[w->nzNm - 1][0])/1e3;
	/* The minimum z is supposed to be the sea level */
	Free2(nm,w->nzNm,dumb);
	/* test */
	/* Printf("Atmosphere init:\nn=%d, dz=%g, zmax=%g\n",
		AtmosphericDensity.n,AtmosphericDensity.dz,AtmosphericDensity.zmax);
	for(i=0;i<100;i++){
		Printf("Nm(%g)=%g\n",i*AtmosphericDensity.dz,AtmosphericDensity.Nm[i]);
	}
	getchar(); */
	/* end test */
}

static void InitNonuniformEfield(World w)
{
	static char Efname[80],/*buf[100],*/xfname[80];
	/* static to allocate in the heap instead of the stack */
	int tmp1,tmp2;
	double *x;
	char *posx,*posE;

	GetNumbers("Charge used to obtain the field in the QE model (C):","f",&(w->qQEH));
	GetNumbers("Electric field data file name (with '?' for 'x'):","s",80,Efname);
	/* GetNumbers("Electric field horizontal and vertical dimension (km):",
		"ff",&(w->xEmax),&(w->zEmax)); */
	if((posE=strchr(Efname,'?'))==NULL) Error("Invalid name (no '?')");
	GetNumbers("Dimensions grid array file name (with '?' for 'x'):","s",80,xfname);
	if((posx=strchr(xfname,'?'))==NULL) Error("Invalid name (no '?')");
	*posx='z';
	/*sprintf(buf,"%sz.dat",xfname);*/
	x=ReadArray(xfname,&(w->nzE));
	w->dzE=(x[1]-x[0])/1e3; /* in km */
	w->zEmax=x[w->nzE-1]/1e3;
	w->zEmin=x[0]/1e3;
	free(x);
	Printf("Read the z array \"%s\"\n",xfname); fflush(stdout);
	/* if(w->type==LAYERED && w->xEmax !=0)
		Error("Horizontal dimension for 1D field is !=0 "); */
	if(w->type==LAYERED){
		/*sprintf(buf,"%sz.dat",Efname);*/
		*posE='z';
		w->E1D=ReadArray(Efname,&tmp1);
		if(tmp1!=w->nzE) Error("file \"%s\" is bad",Efname);
		Printf("Read the Ez array \"%s\"\n",Efname); fflush(stdout);
		/* Convert to dimensionless */
		/*for(i=0;i<w->nzE;i++) w->E1D[i]*=scale;*/
		/* Zero the unused stuff */
		w->nxE=0;
		w->Ex=w->Ez=NULL;
		w->dxE=-1; /* negative to indicate its absence */
	}else{
		/* 2D field */
		w->E1D=NULL; /* zero the unused stuff */
		/* Read the horizontal dimensions */
		if(w->type==CARTESIAN)
			*posx='x';
			/*sprintf(buf,"%sx.dat",xfname);*/
		else if(w->type==CYLINDRICAL)
			*posx='r';
			/*sprintf(buf,"%sr.dat",xfname);*/
		else
			Error("InitNonuniformWorld: unknown type");
		x=ReadArray(xfname,&(w->nxE));
		w->dxE=(x[1]-x[0])/1e3; /* in km */
		w->xEmax=x[w->nxE-1]/1e3;
		w->xEmin=x[0]/1e3;
		if(w->type==CYLINDRICAL && w->xEmin!=0)
			Error("InitNonuniformEfield: Non-zero minimum r");
		free(x);
		Printf("Read the horiz. array \"%s\"\n",xfname); fflush(stdout);
		/* Read the field */
		/* horizontal */
		if(w->type==CARTESIAN)
			*posE='x';
			/*sprintf(buf,"%sx.dat",Efname);*/
		else if(w->type==CYLINDRICAL)
			*posE='r';
			/*sprintf(buf,"%sr.dat",Efname);*/
		else
			Error("InitNonuniformWorld: unknown type");
		w->Ex=ReadMatrix(Efname,&tmp1,&tmp2);
		if(tmp1!=w->nxE || tmp2!=w->nzE)
			Error("InitNonuniformEfield: file \"%s\" is bad",Efname);
		Printf("Read the horiz. field \"%s\"\n",Efname); fflush(stdout);
		/* vertical */
		*posE='z';
		/*sprintf(buf,"%sz.dat",Efname);*/
		w->Ez=ReadMatrix(Efname,&tmp1,&tmp2);
		if(tmp1!=w->nxE || tmp2!=w->nzE)
			Error("InitNonuniformEfield: file \"%s\" is bad",Efname);
		Printf("Read the z field \"%s\"\n",Efname); fflush(stdout);
		/* Convert to dimensionless */
		/*for(i=0;i<w->nxE;i++){
			for(j=0;j<w->nzE;j++){
				w->Ex[i][j]*=scale;
				w->Ez[i][j]*=scale;
			}
		}*/
	}
}

void UpdateNonUniformWorld(World w,double q)
{
	w->Escale=q/(w->qQEH)*586.679; /* to convert to inverse seconds */
}

void UpdateUniformWorld(World w,double delta0, double eta0, double mu0)
{
	double Et,Efield;

	if(w->type!=UNIFORM) Error("Cannot update a non-uniform world");
	/* Threshold field */
	Et=21.7331; // = tau * e*Et/(mc) where 1/tau = 2*pi*c*Nm*Zm*r0**2
	/* Initiate global variables */
	w->useE=TRUE;
	w->EabsUniform=Efield=delta0*Et; /* in 1/sec */
	w->BabsUniform=w->BzUniform=eta0*Et; /* in 1/sec */
	w->BxUniform=w->ByUniform=0;
	if(mu0<-1 || mu0>1)
		Error("invalid angle");
	w->ExUniform=Efield*sqrt(1-mu0*mu0);
	w->EzUniform=Efield*mu0;
	Printf("In inverse seconds: Ex=%g, Ez=%g, B=%g\n",
		w->ExUniform,w->EzUniform,w->BabsUniform);
	fflush(stdout);
}

void GetE(World w,double x, double y, double z, double *Ex, double *Ey,double *Ez)
{
	double Er,r;

	if(!w->useE){
		*Ex=*Ey=*Ez=0;
		return;
	}
	switch(w->type){
	case UNIFORM:
		*Ex=w->ExUniform;
		*Ey=0;
		*Ez=w->EzUniform;
		return;
	/* For a non-uniform world, the field has to be multiplied by a coefficient */
	case LAYERED:
		/* Only vertical field */
		if(z<0.){
			*Ex=0; *Ey=0; *Ez=w->E1D[0]*(w->Escale);
		}else if(z > w->zEmax){
			*Ex=0; *Ey=0; *Ez=0;
		}else{
			*Ex=0; *Ey=0; *Ez=Interpolate1(w->E1D,w->nzE,w->dzE,z)*(w->Escale);
		}
		break;
	case CARTESIAN:
		if(x > w->xEmax || z > w->zEmax || z<0 || x<0){
			*Ex=*Ey=*Ez=0; break;
		}
		*Ex=Interpolate2(w->Ex,w->nxE,w->nzE,w->dxE,w->dzE,x,z)*(w->Escale);
		*Ey=0;
		*Ez=Interpolate2(w->Ez,w->nxE,w->nzE,w->dxE,w->dzE,x,z)*(w->Escale);
		break;
	case CYLINDRICAL:
		r=sqrt(x*x+y*y);
		if(r > w->xEmax || z > w->zEmax || z<0){
			*Ex=*Ey=*Ez=0; break;
		}
		Er=Interpolate2(w->Ex,w->nxE,w->nzE,w->dxE,w->dzE,r,z)*(w->Escale);
		*Ez=Interpolate2(w->Ez,w->nxE,w->nzE,w->dxE,w->dzE,r,z)*(w->Escale);
		if(r>1e-1){ /* non-zero radius */
			*Ex=x/r*Er; *Ey=y/r*Er;
		}else{
			*Ex=*Ey=0;
		}
		break;
	default:
		Error("GetE: Unknown geometry type");
		break;
	}
}

double GetEabs(World w,double x,double y,double z)
{
	double Ex,Ey,Ez;
	if(!w->useE) return 0;
	if(w->type==UNIFORM) return w->EabsUniform;
	GetE(w,x,y,z,&Ex,&Ey,&Ez);
	return sqrt(Ex*Ex+Ey*Ey+Ez*Ez);
}

double GetNm(World w, double z)
{
	/* z is in km */
	double Nm;
	if(w->type==UNIFORM) return 1;
	if(z<0.) Nm=w->Nm[0];
	else if(z>w->zNmmax) Nm=0;
	else Nm=Interpolate1(w->Nm,w->nzNm,w->dzNm,z);
	return Nm;
}

/*
 * Total atmospheric contents functions.
 *
 * InitTAC (private)
 * -------
 * Calculate the total atmospheric contents and store it in array w->TAC
 * (when the density w->Nm is already known).
 * Note that Nm is already in units of NmConv, so the TAC will be in units
 * of NmConv*km.
 *
 * GetTACfromZ, GetZfromTAC
 * ------------------------
 * Total atmospheric contents from altitude and
 * altitude from total atmospheric contents.
 * "Atmosphere" is any non-uniform medium, with density stored in w->Nm.
 */
static void InitTAC(World w)
{
	int i;
	Printf("Initializing the total atmospheric contents ... "); fflush(stdout);
	w->TAC=Allocate1(w->nzNm);
	/* TAC==0 at maximum altitude */
	w->TAC[w->nzNm-1]=0;
	/* TAC is calculated by integrating the linear interpolation of Nm */
	for(i=w->nzNm-2; i>=0; i--){
		w->TAC[i]=w->TAC[i+1]+(w->Nm[i]+w->Nm[i+1])/2*w->dzNm;
	}
	Printf("done\n"); fflush(stdout);
	/* For debugging */
	/*for(i=0; i<w->nzNm; i++){
		Printf("%12g\t%12g\t%12g\n",i*w->dzNm,w->Nm[i],w->TAC[i]);
	}
	Printf("zmax=%g\n",w->zNmmax);
	*/
}

double GetTACfromZ(World w,double z)
{
	double TAC;
	if(z<0.) TAC=w->TAC[0];
	else if(z>w->zNmmax) TAC=0;
	else TAC=Interpolate1(w->TAC,w->nzNm,w->dzNm,z);
	return TAC;
}

double GetZfromTAC(World w,double TAC)
{
	double z,dtac,tac;
	int ih,il,im;
	if(TAC >= w->TAC[0]) return 0;
	if(TAC <= 0) return w->zNmmax;
	/* Bisectional search, using the fact that
	   TAC decreases monotonically with altitude */
	il=0; ih=w->nzNm-1;
	while(ih-il>1){
		im=(il+ih)/2;
		/* Printf("il=%d,im=%d,ih=%d",il,im,ih); getchar(); */
		if(w->TAC[im]<TAC) ih=im;
		else il=im;
	}
	/* Now ih-il=1 and TAC is located between them */
	dtac=w->TAC[il] - w->TAC[ih];
	tac=TAC - w->TAC[ih];
	z=(w->dzNm * ih * (dtac-tac) + w->dzNm * il * tac) / dtac;
	/* Printf("GetAltitude(%g)=%g=>%g",TAC,z,GetTAC(z)); getchar(); */
	return z;
}

double GetMaxTAC(World w)
{
	return w->TAC[0];
}

void GetB(World w,double *Bx,double *By,double *Bz)
{
	*Bx=w->BxUniform;
	*By=w->ByUniform;
	*Bz=w->BzUniform;
}

double GetBabs(World w)
{
	return w->BabsUniform;
}

bool IsUniform(World w)
{
	return (w->type==UNIFORM);
}

bool UseE(World w)
{
	return w->useE;
}

double GetScaleHeight(World w)
{
	if(w->type==UNIFORM)
		Error("GetScaleHeight: no scale height for uniform world");
	return w->scaleHeight;
}

/*
 * IsInside
 * -----------
 * Determine whether the point is located inside the system.
 */
bool IsInside(World w,double x,double y,double z)
{
	if(w->type==UNIFORM) return TRUE;
	if(z<=0.001){		//Particle has reached ground
		//Error("Warning: OutOfBounds: landing");
		return FALSE;
	}
	if(z>w->zNmmax){
		if(!w->useE){
			return FALSE;
		}else{ /* w->useE==TRUE */
			if(z>w->zEmax) return FALSE;
		}
	}
	if(w->type==LAYERED || !w->useE) return TRUE;
	/* 2D */
	/* Now we know for sure that w->useE==TRUE */
	if(w->type==CARTESIAN){
		if(x>w->xEmin && x<w->xEmax) return TRUE;
		else return FALSE;
	}
	/* CYLINDRICAL */
	if(sqrt(x*x+y*y) > w->xEmax) return FALSE;
	return TRUE;
}

/*
 * BoundaryIntersect
 * -----------------
 * If the particle continues to go in the given direction, where will it
 * intersect the boundary of the world ?
 */
bool BoundaryIntersect(World w,double x0,double y0,double z0,
	double vx,double vy,double vz,double *tp)
{
	if(w->type==UNIFORM) return FALSE;
	if(w->type==CARTESIAN || w->type==CYLINDRICAL || w->useE){
		Error("For this geometry, BoundaryIntersect is not implemented (yet)");
	}
	/* LAYERED */
	if(vz>0){
		*tp=(w->zNmmax-z0)/vz;
		return TRUE;
	}else if(vz<0){
		*tp=-z0/vz; /* zmin=0 */
		return TRUE;
	}else{/* vz==0 */
		return FALSE;
	}
}

/*********** DEPOSITED ENERGY CALCULATIONS ***********/
void InitDepositEnergy(World w)
{
	if(w->type==UNIFORM)
		Error("We do not calculate deposited energy for uniform simulation");
	w->endep=Allocate1(w->nzNm);
}

void ClearDepositEnergy(World w)
{
	int i;

	if(w->type==UNIFORM)
		Error("We do not calculate deposited energy for uniform simulation");
	for(i=0; i<w->nzNm; i++) w->endep[i]=0;
}

void DepositEnergy(World w,double endep,double z1,double z0)
{
	double z;
	int i;
#if !SIMPLE_DEPOSIT_ENERGY
	double dz;
	int i0,i1;
#endif
#if DEBUG >= 2
	Printf("Deposit(%lf)\n",endep);
#endif
	if(w->type==UNIFORM)
		Error("We do not calculate deposited energy for uniform simulation");
#if SIMPLE_DEPOSIT_ENERGY
	/* Simplest way -- deposit all in the middle point */
	z=(z0+z1)/2;
	i=(int)floor(z/w->dzNm);
	if(i<0 || i>=w->nzNm)
		Error("DepositEnergy: out of range, z={%g,%g}\n",z0,z1);
	w->endep[i] += endep;
#else
	if(z0>z1){z=z0; z0=z1; z1=z;}/* Now z1>z0*/
	i0=(int)floor(z0/w->dzNm);
	i1=(int)floor(z1/w->dzNm);
	if(i0<0 || i0>=w->nzNm || i1<0 || i1>=w->nzNm){
		Printf("dz=%g,i={%d,%d},nz=%d\n",w->dzNm,i0,i1,w->nzNm);
		Error("DepositEnergy: out of range, z={%g,%g}\n",z0,z1);
	}
	if(i0==i1){
		w->endep[i0] += endep;
	}else{
		dz=z1-z0;
		w->endep[i0] += endep * ((i0+1)*w->dzNm - z0)/dz;
		w->endep[i1] += endep * (z1-i1*w->dzNm)/dz;
		for(i=i0+1; i<i1; i++) w->endep[i] += endep * w->dzNm/dz;
	}
#endif
}

void OutputDepositEnergy(World w,char *fname)
{
	FILE *file;
	int i;

	file=Open(fname,"w");
	if(w->type==UNIFORM)
		Error("We do not calculate deposited energy for uniform simulation");
	for(i=0; i<w->nzNm; i++){
		/* Print altitude in m, energy in eV/m (or keV/km) */
		fprintf(file,"%14.5e\t%14.5e\n",i*w->dzNm*1e3,w->endep[i]*511);
	}
	fclose(file);
}

double GetTotalDepositedEnergy(World w)
{
	int i;
	double en=0;
	if(w->type==UNIFORM)
		Error("We do not calculate deposited energy for uniform simulation");
	for(i=0; i<w->nzNm; i++) en += w->endep[i];
	return en;
}
