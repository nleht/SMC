/*
 * File: fid.c
 * -----------
 * FID stands for "fill initial distribution"
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "fid.h"
#include "interface.h"
#include "util.h"
#include "particle.h"
#include "mystack.h"
	/* -- for "memcpy" */

typedef enum {MANUAL,SINGLE_FILE,MULTIPLE_FILES,DISTRIBUTION} ChoiceT;
static void *ReadRecordFunction(FILE *file,void *e);

struct FIDCDT {
	int numDistr; /* how many different initial distributions */
	ChoiceT choice;
	bool isMultiple;
	int iRun; /* the number of the run */
	unsigned long numDifferent; /* number of different electrons */
	char fname[100]; /* file name for predefined distribution */
	unsigned long multiplicity; /* how many electrons have the same momentum */
	/* double **allDistr;*/ /* for choice==SINGLE_FILE or MANUAL, the initial distribution for all runs */
	Stack allDistr; /* now - a stack of particles instead of 2D array */
	/*double **currentDistr;*/ /* initial distribution for the current run */
	Stack currentDistr; /* now - a stack */
	unsigned long nextDifferent,nextSame;
};

FIDT InitFID(void)
{

//May need to modify here to implement random directions and energies

	FIDT fid;
	Particle tmpParticle;
	int choice,i,j,ind;
	double angle, minEnergy, maxEnergy, beamWidth;
	double weight,x,y,z;
	int kind;
	/* int k; */

	if(!(tmpParticle=malloc(ParticleMemUsage())))
		Error("InitFID: Not enough memory 1");
	if(!(fid=malloc(sizeof(struct FIDCDT))))
		Error("InitFID: Not enough memory 2");
	/* Initialize fid->choice */
	GetNumbers("Distribution: 1-manual,2-single,3-multiple files,4-1/E distribution:","d",&choice);
	switch(choice){
	case 1:
		fid->choice=MANUAL;
		break;
	case 2:
		fid->choice=SINGLE_FILE;
		break;
	case 3:
		fid->choice=MULTIPLE_FILES;
		break;
	case 4:
		fid->choice=DISTRIBUTION;
		break;
	default:
		Error("Invalid choice: %d",choice);
	}
	GetNumbers("How many different initial distributions?","d",&(fid->numDistr));
	GetNumbers("Number of initial particles with same momentum and coordinates:",
		"d",&(fid->multiplicity));
	if(fid->multiplicity<=0)
		Error("InitFID: Invalid parameters: multiplicity=%d<=0",fid->multiplicity);	
	
	/* Initialize fid->currentDistr */
	switch(fid->choice){
	case MANUAL:
	case SINGLE_FILE:
	case DISTRIBUTION:
		GetNumbers("Number of different particles:","d",&(fid->numDifferent));
		/* fid->currentDistr=Allocate2(fid->numDifferent,INFO_LENGTH);*/
		fid->currentDistr=NewStack(fid->numDifferent,ParticleMemUsage());
		/*Printf("allocated currentDistr (%d x %d)\n",fid->numDifferent,INFO_LENGTH);*/
		break;
	case MULTIPLE_FILES:
		fid->currentDistr=NULL;
		break;
	default:
		Error("invalid choice");
		break;
	}
	
	/* Initialize fid->allDistr */
	switch(fid->choice){
	case MANUAL:
		/* fid->allDistr=Allocate2((fid->numDifferent)*(fid->numDistr),INFO_LENGTH); */
		fid->allDistr=NewStack((fid->numDifferent)*(fid->numDistr),ParticleMemUsage());
		/*Printf("allocated allDistr (%dx%d)\n",(fid->numDifferent)*(fid->numDistr),INFO_LENGTH);*/
		GetNumbers("Enter particle information: kind, weight, dimensionless momenta, coordinates (km):","");
		for(j=0; j<fid->numDistr;j++){
			for(i=0;i<fid->numDifferent;i++){
				ind=i+j*(fid->numDifferent);
				ReadParticleLineFromConsole(tmpParticle);
				Push(fid->allDistr,tmpParticle);
			}
		}
		break;
	case SINGLE_FILE:
		GetNumbers("Name of the single file with all distributions:","s",80,fid->fname);
		/* read information from a file */
		/*fid->allDistr=NewStack((fid->numDifferent)*(fid->numDistr),ParticleMemUsage());*/
		fid->allDistr=ReadStack(fid->fname,ParticleMemUsage(),ReadRecordFunction);
		if(GetNumStack(fid->allDistr)!=(fid->numDifferent)*(fid->numDistr)){
			Printf("GetNumStack(fid->allDistr) = %d, (fid->numDifferent)=%d, (fid->numDistr)=%d\n",GetNumStack(fid->allDistr),(fid->numDifferent),(fid->numDistr));
			Error("InitFID: invalid distributions file");
		}
		break;
	case MULTIPLE_FILES:
		GetNumbers("Name of the distrib. file (without \"_n.dat\"):","s",80,fid->fname);
		fid->allDistr=NULL; 
		break;
	case DISTRIBUTION:
		GetNumbers("Zenith angle and width of beam in deg","ff",&angle,&beamWidth);
		angle=angle/180*PI; beamWidth=beamWidth/180.*PI;
		GetNumbers("Range of Energy in keV","ff",&minEnergy,&maxEnergy);
		minEnergy=minEnergy/511; maxEnergy=maxEnergy/511;
		fid->allDistr=NewStack((fid->numDifferent)*(fid->numDistr),ParticleMemUsage());
		GetNumbers("Enter particle information: kind, weight, (x,y,z) coordinates (km):","");
		for(j=0;j<fid->numDistr;j++){
			GetNumbers("","dffff",&kind,&weight,&x,&y,&z);
			for(i=0; i<fid->numDifferent; ++i)
			{
				//generate one random line.
				GetRandomlyDistributedParticle(tmpParticle,beamWidth,angle,minEnergy,maxEnergy,kind,weight,x,y,z);
				Push(fid->allDistr,tmpParticle);
			}
		}
		// Printf("Just created fid->allDistr: \n");
		// StackContents(fid->allDistr,PrintParticleInfo);
		break;
	default:
		Error("Invalid distribution choice: %d",fid->choice);
		break;
	}
	fid->iRun=-1; /* Number of the run is not known */
	free(tmpParticle);
	return fid;
}

int GetNumberOfInitialDistributions(FIDT fid)
{
	/*Printf("GetNumberOfInitialDistributions: %d\n",fid->numDistr);*/
	return fid->numDistr;
}

void ResetFID(FIDT fid,int iRun, bool verbose)
{
	static char buf[100]; /* static to allocate in the heap */
	int i,ind;
	
	fid->iRun=iRun;
	fid->nextDifferent=0;
	fid->nextSame=0;
	if (verbose) { Printf("BEGIN ResetFID(fid,iRun=%d)\n",iRun); fflush(stdout); }
	switch(fid->choice){
	case MANUAL:
	case SINGLE_FILE:
	case DISTRIBUTION:
		//Printf("*** fid->allDistr ***\n");
		//StackContents(fid->allDistr,PrintParticleInfo);
		ClearStack(fid->currentDistr);
		/* We already loaded all distributions in fid->allDistr,
		   now extract them to fid->currentDistr */
		for(i=0; i<fid->numDifferent; i++){
			ind=(fid->iRun)*(fid->numDifferent)+i;
			/* Copy the particle info */
			Push(fid->currentDistr,GetElementByIndex(fid->allDistr,ind));
		}
		break;
	case MULTIPLE_FILES:
		sprintf(buf,"%s_%d.dat",fid->fname,fid->iRun);
		fid->currentDistr=ReadStack(buf,ParticleMemUsage(),ReadRecordFunction);
		fid->numDifferent=GetNumStack(fid->currentDistr);
		break;
	default:
		Error("ResetFID: unknown choice");
		break;
	}
	if (verbose) { Printf("END ResetFID\n"); fflush(stdout); }
}

/* For debugging */
void FIDContents(FIDT fid)
{
	int i;
	
	Printf(">>>Initial distribution:\n");
	Printf("Number of different electrons = %d\n",(int)fid->numDifferent);
	if(GetNumStack(fid->currentDistr)!=fid->numDifferent)
		Error("Different number of elements");
	Printf("Multiplicity = %d\n",(int)fid->multiplicity);
	for(i=0; i<fid->numDifferent; i++)
		PrintParticleInfo(GetElementByIndex(fid->currentDistr,i));
	Printf("<<<\n");
	getchar();
}

bool CopyNextFromFID(FIDT fid,Particle e)
{
	// Printf("CopyNextFromFID: index=%d\n",fid->nextDifferent + fid->iRun*fid->numDifferent);
	if(fid->nextDifferent >= fid->numDifferent) return FALSE;
	memcpy(e,GetElementByIndex(fid->currentDistr,fid->nextDifferent),ParticleMemUsage());
	fid->nextSame++;
	if(fid->nextSame >= fid->multiplicity){
		fid->nextSame=0;
		fid->nextDifferent++;
	}
	return TRUE;
}

static void *ReadRecordFunction(FILE *file,void *e)
{
	return (void *)ReadParticleLineFromFile(file,(Particle)e);
}


