/*
 * File: world.h
 * -------------
 */
#ifndef _world_h_
#define _world_h_

/* Incomplete type for the world */
typedef struct WorldCDT *World;

World InitWorld(void);
void UpdateNonUniformWorld(World w,double q);
void UpdateUniformWorld(World w,double delta0, double eta0, double mu0);
void GetE(World w,double x, double y, double z, double *Ex, double *Ey,double *Ez);
double GetEabs(World w,double x,double y,double z);
double GetNm(World w, double z);
/* Total atmospheric contents functions */
double GetTACfromZ(World w,double z);
double GetZfromTAC(World w,double TAC);
double GetMaxTAC(World w);
void GetB(World w,double *Bx,double *By,double *Bz);
double GetBabs(World w);
bool IsUniform(World w);
bool UseE(World w);
double GetScaleHeight(World w);
bool IsInside(World w,double x,double y,double z);
bool BoundaryIntersect(World w,double x0,double y0,double z0,
	double vx,double vy,double vz,double *tp);
void InitDepositEnergy(World w);
void ClearDepositEnergy(World w);
void DepositEnergy(World w,double endep,double z0,double z1);
void OutputDepositEnergy(World w,char *fname);
double GetTotalDepositedEnergy(World w);

#endif /* _world_h_ */
