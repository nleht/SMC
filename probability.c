/*
 * rnwyprob
 * --------
 * This project is supposed to calculate propabilities of electron to start
 * a runaway avalanche.
 * 
 * History:
 *
 * 2/20/17
 * Now it uses all the subroutines from the regular MC code. Old version retired.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "util.h"
#include "storage.h"

/* Debugging flags */
#define DEBUG_DT	0

/* Printing preferences */
struct {
	bool probToStart,probToBecome;
} NEED;

/* PROB - where we are going to store the calculated probabilities for selected points */
struct {
	long ntryToStart,ntryToBecome; /* number of tries to estimate the probability either
		to start an avalanche, or to become a runaway */
	long np,nth; /* number of coordinates */
	double *p, *th;   /* points coordinates */
	double **toStart,**toBecome; /* the result */
} PROB;

/************************** Prototypes **********************/

/* Initialization functions */
int Main(void);


void RunProbabilitySimulation(Simulation s)
{
	if(IsUniform(s->world)){
		InitDimensionlessParameters(s);
	}
	StartCalculations();
	if(IsUniform(s->world)){
		RunMultipleUniformSimulation(s);
	}else{
		Printf("Non-uniform\n");
		RunMultipleNonUniformSimulation(s);
	}
}


int RunProbSimulation(
		int numIniEls,
		long dotPrintStep,	/* colsole output prefs */
		double Emin,
		double px,double py,double pz,	/* initial beam information */
		double delta0,double eta0,double mu0,	/* dimensionless parameters */
		/* when to exit - on achieving the
		   desired number or the desired momentum */
		int numFinEls, double pFinal,
		bool exitOnNumber); 

/**************** START OF THE PROGRAM ********************/

int Main(void)
{
	int numFinEls,finalElectronNumber;
	double pFinal;
	double Emin,px,py,pz;
	int ndelta0,neta0,nmu0;
	int idelta0,ieta0,imu0;
	int ideltaini,ietaini,imuini;
	double *Delta0,*Eta0,*Mu0;
	long dotPrintStep; /* for output to the terminal */
	long maxNumElectrons;
	FILE *logFile;
	/* Probability calculations */
	int ip,ith,itry,sum;
	static char psfileName[256],pbfileName[256];
	FILE *psFile,*pbFile;
	
	/* Initialize array for runaway electrons */
	GetNumbers("Maximum number of electrons:","l",&maxNumElectrons);
	RunawayStorage=NewStorage(maxNumElectrons,sizeof(RELrec));
	/* Field and other information */
	GetNumbers("Minimum electron energy that we consider (keV):","f",&Emin);
	Emin/=511; /* convert to dimensionless */

	GetNumbers("Final number of electrons:","l",&numFinEls);
	GetNumbers("Final electron momentum (dimensionless):","f",&pFinal);
	GetNumbers("Dot print step:","l",&dotPrintStep);
	InitMyRandom();
	InitPreferences();
	InitTime();
	InitProbability();
	InitPrinting();
	/* Input the table of dimensionless parameters delta0,eta0,mu0 */
	GetNumbers("Number of deltas:","d",&ndelta0);
	if(ndelta0>0){
		Delta0=Allocate1(ndelta0);
		GetNumbers("delta0 values:","");
		for (idelta0=0;idelta0<ndelta0;idelta0++)
			GetNumbers("","f",&Delta0[idelta0]);
	}
	GetNumbers("Number of etas:","d",&neta0);
	if(neta0>0){
		Eta0=Allocate1(neta0);
		GetNumbers("eta0 values:","");
		for (ieta0=0;ieta0<neta0;ieta0++)
			GetNumbers("","f",&Eta0[ieta0]);
	}
	GetNumbers("Number of mus:","d",&nmu0);
	if(nmu0>0){
		Mu0=Allocate1(nmu0);
		GetNumbers("mu0 values:","");
		for (imu0=0;imu0<nmu0;imu0++)
			GetNumbers("","f",&Mu0[imu0]);
	}
	GetNumbers("Initial numbers to tag delta0, eta0, and mu0:",
		"ddd",&ideltaini,&ietaini,&imuini);
	/* For each combination of delta0,eta0 and mu0, run the Monte Carlo simulation 
	   (many times) */
	for(idelta0=0;idelta0<ndelta0;idelta0++){
		for(ieta0=0;ieta0<neta0;ieta0++){
			for(imu0=0;imu0<nmu0;imu0++){
				/* Files to save the information about the probabilities */
				if(NEED.probToStart)
					sprintf(psfileName,"data/ps%d_%d_%d.dat",
						idelta0+ideltaini,ieta0+ietaini,imu0+imuini);
				if(NEED.probToBecome)
					sprintf(pbfileName,"data/pb%d_%d_%d.dat",
						idelta0+ideltaini,ieta0+ietaini,imu0+imuini);
				/* Go over different momentum and angle values */
				for(ip=0;ip<PROB.np;ip++){
					for(ith=0;ith<PROB.nth;ith++){
						px=PROB.p[ip]*sin(PROB.th[ith]);
						py=0;
						pz=-PROB.p[ip]*cos(PROB.th[ith]);
						printf("\nNew runs with p = %f, th = %f deg\n",PROB.p[ip],PROB.th[ith]*180/PI);
						/* 1. Calculate probability to start a raunaway avalanche
						   by repeatedly running the MC simulation with 1 electron */
						if(NEED.probToStart){
							PREF.create=TRUE; /* we need avalanche */
							for(itry=0,sum=0;itry<PROB.ntryToStart;itry++){
								finalElectronNumber = RunSimulation(
									1, /* start with a single electron */
									dotPrintStep,
									Emin,
									px,py,pz,
									Delta0[idelta0],Eta0[ieta0],Mu0[imu0],
									numFinEls,pFinal,TRUE); /* exit on achieving numFinEls
															   or on stopping the avalanche */
								/* printf("<%d>",finalElectronNumber); */
								sum += (finalElectronNumber>0) ? 1 : 0;
							}
							/* Probability estimate */
							PROB.toStart[ip][ith]=((double)sum)/((double)PROB.ntryToStart);
							printf("\nProbability to start = %f\n",PROB.toStart[ip][ith]);
						}
						/* 2. Calculate probablitity to become a runaway by starting with
						   several electrons and seeing how many survive */
						if(NEED.probToBecome){
							PREF.create=FALSE; /* no new electrons are created,
												  so that we don't mix them with initial */
							finalElectronNumber = RunSimulation(
									PROB.ntryToBecome,
									dotPrintStep,
									Emin,
									px,py,pz,
									Delta0[idelta0],Eta0[ieta0],Mu0[imu0],
									PROB.ntryToBecome+1, /* we never reach this number of electrons */
									pFinal,FALSE); /* exit on all electrons
													  achieving final momentum pFinal
													  or on stopping the avalanche */
							/* printf("\n# of runaways = %d\n",finalElectronNumber); */
							PROB.toBecome[ip][ith]=
								((double)finalElectronNumber)/((double)PROB.ntryToBecome);
							printf("\nProbability to become = %f\n",PROB.toBecome[ip][ith]);
						}
						/* Print out probability information */
						/* Serious bug: the file might contain previous run's information! */
						if(NEED.probToStart){
							psFile=Open(psfileName,"a");
							fprintf(psFile,"%7.3f\t",PROB.toStart[ip][ith]);
							fclose(psFile);
						}
						if(NEED.probToBecome){
							pbFile=Open(pbfileName,"a");
							fprintf(pbFile,"%7.3f\t",PROB.toBecome[ip][ith]);
							fclose(pbFile);
						}
					} /* end of cycle over ith */
					/* Print the linefeeds */
					if(NEED.probToStart){
						psFile=Open(psfileName,"a");
						fprintf(psFile,"\n");
						fclose(psFile);
					}
					if(NEED.probToBecome){
						pbFile=Open(pbfileName,"a");
						fprintf(pbFile,"\n");
						fclose(pbFile);
					}
				} /* end of cycle over ip */
				/* Make a record about the run */
				logFile=Open("data/log.dat","a");
				fprintf(logFile,"%10d\t%10d\t%10d\t%12.3e\t%12.3e\t%12.3e\n",
					idelta0+ideltaini,ieta0+ietaini,imu0+imuini,
					Delta0[idelta0],Eta0[ieta0],Mu0[imu0]);
				fclose(logFile);
			} /* end of cycle over imu0 */
		} /* end of cycle over ieta0 */
	} /* end of cycle over idelta0 */
	FreeStorage(RunawayStorage);
	return 0;
}

/*
 * InitProbability
 * ---------------
 * Fill out some fields in the PROB structure.
 */
void InitProbability(void)
{
	double pmin,pmax,thmin,thmax;
	int ip,ith;
	FILE *file;
	
	GetNumbers("Try how many times for electron to start an avalanche?","l",&PROB.ntryToStart);
	GetNumbers("Try how many times for electron to become a runaway?","l",&PROB.ntryToBecome);
	GetNumbers("Number of momentum values, min and max values (in mc units):",
		"lff",&PROB.np,&pmin,&pmax);
	if(PROB.np<1) Error("number of momentum values == %ld < 1",PROB.np);
	PROB.p=Allocate1(PROB.np);
	if(PROB.np==1){
		PROB.p[0]=pmin; /* only 1 value */
	}else{
		for(ip=0;ip<PROB.np;ip++)
			PROB.p[ip]=exp(log(pmin)+ip*log(pmax/pmin)/(PROB.np-1));
	}
	GetNumbers("Number of polar angles (theta), min and max values (in degrees):",
		"lff",&PROB.nth,&thmin,&thmax);
	if(PROB.nth<1) Error("Number of theta values == %ld < 1",PROB.nth);
	thmin*=PI/180.; thmax*=PI/180.; /* convert to radians */
	PROB.th=Allocate1(PROB.nth);
	if(PROB.nth==1){
		PROB.th[0]=thmin; /* only 1 value */
	}else{
		for(ith=0;ith<PROB.nth;ith++)
			PROB.th[ith]=thmin + ith * (thmax-thmin)/(PROB.nth-1);
	}
	PROB.toStart=Allocate2(PROB.np,PROB.nth);
	PROB.toBecome=Allocate2(PROB.np,PROB.nth);
	/* Dump information about momenta and angles */
	file=Open("data/momentum.dat","w");
	for(ip=0;ip<PROB.np;ip++)
		fprintf(file,"%14.5e\n",PROB.p[ip]);
	fclose(file);
	file=Open("data/angle.dat","w");
	for(ith=0;ith<PROB.nth;ith++)
		fprintf(file,"%14.5e\n",PROB.th[ith]);
	fclose(file);	
}

/*
 * InitPrinting
 * ------------
 * Initialize printing preferences.
 */
void InitPrinting(void)
{
	GetNumbers("Print probability to start an avalanche (y/n)?","b",&NEED.probToStart);
	GetNumbers("Print probability to become a runaway (y/n)?","b",&NEED.probToBecome);
}

/*
 * RunSimulation
 * -------------
 * This is our principal routine.
 * Modification on 7/16/97:
 * now can exit on achieving certain value of momentum. Return number of electrons.
 * Modification on 7/11/97: simplify for "rnwyprob" project:
 * Hard code # of initial electrons (i.e. one),
 * Remove output,
 * Return the boolean value which shows the fact of avalanche start.
 */
int RunProbSimulation(
		int numIniEls,
		long dotPrintStep,	/* colsole output prefs */
		double Emin,
		double px,double py,double pz,	/* initial beam information */
		double delta0,double eta0,double mu0,	/* dimensionless parameters */
		/* when to exit - on achieving the
		   desired number or the desired momentum */
		int numFinEls, double pFinal,
		bool exitOnNumber)
{
	double dt,Et,Efield,Ex,Ez,B;
	RELrec *electron,*newElectron;
	static long iDotPrint=0,iDotLinePrint=0;
	bool timeToExit;

	/* Threshold field */
	Et=21.7331;
	/* Initiate global variables */
	Efield=delta0*Et; /* in 1/sec */
	B=eta0*Et; /* in 1/sec */
	if(mu0<-1 || mu0>1)
		Error("invalid angle");
	Ex=Efield*sqrt(1-mu0*mu0);
	Ez=Efield*mu0;	
	FillInitialDistribution(numIniEls,px,py,pz);

	/* Start the main cycle */
	dt=GetDt(Emin,Efield,B);
	
	for(T.t=0; T.t<T.end; T.t+=dt){
		/* The main thing starts here */
		ResetStorage(RunawayStorage);
		while((electron=NextInStorage(RunawayStorage))!=NULL){
			/* Output to console -- required on Mac to be able
			   to do sth else on the same computer */
			iDotPrint++;
			if(iDotPrint==dotPrintStep){
				printf("."); fflush(stdout); iDotPrint=0;
				iDotLinePrint++;
				/* Check whether we need to advance the line */
				if(iDotLinePrint==40){
					printf("\n"); fflush(stdout); iDotLinePrint=0;
				}
			}
			/* End output to console */
			newElectron=Step(electron,dt,B,Ex,Ez,Emin);
			/* Kill electrons with less than Emin kinetic energy */
			if(electron->g - 1 < Emin){
				/* printf("(%d-1)",GetNumStorage(RunawayStorage)); */
				RemoveFromStorage(RunawayStorage,electron);
			}
			if(newElectron){
				if(!AddToStorage(RunawayStorage,newElectron))
					Error("Storage overflow");
			}
		}
		/* The main thing finishes here */

		  		
		/* EXITING */
		
  		/* Check if we have no avalanche. If no electrons left, exit. */
  		if(GetNumStorage(RunawayStorage)==0){
  			/* No avalanche */
			return 0;
		}
		
 		/* Exit if we have the required number of electrons */
 		if(exitOnNumber && GetNumStorage(RunawayStorage)>=numFinEls)
  			return GetNumStorage(RunawayStorage);
  		
  		/* Exit if electrons all have momentum > pFinal */
  		if(!exitOnNumber){
  			ResetStorage(RunawayStorage);
  			timeToExit=TRUE;
			while((electron=NextInStorage(RunawayStorage))!=NULL){
				if(electron->p < pFinal){
					timeToExit=FALSE;
					break;
				}
			}
			if(timeToExit)
				return GetNumStorage(RunawayStorage);
		}
		/* END EXITING */
		
	}
	Error("Time elapsed, number of electrons = %ld\n",GetNumStorage(RunawayStorage));
	return 0;
}

