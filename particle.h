/*
 * File: particle.h
 * ----------------
 */

#ifndef _particle_h_
#define _particle_h_

#include "world.h"

/* Incomplete type for the world */
typedef struct ParticleCDT *Particle;

/* The particle kind: we consider electrons, positrons or photons */
#define NUMPARTICLEKINDS 3;
typedef enum {ELECTRONKIND=-1,PHOTONKIND,POSITRONKIND} ParticleKindT;
/* The status of the particle after a time step */
typedef enum {NOTHING,DISAPPEAR,EXPIRE,LEAVE,DOUBLE} ParticleStatusT;

bool GetParticleClassProperty(char *property);

size_t ParticleMemUsage(void);
/* Get and Set */
void SetParticleKind(Particle e,ParticleKindT kind);
void SetParticleMomentum(Particle e, double px, double py, double pz);
void SetParticleCoordinates(Particle e, double x, double y, double z);
void SetParticleTime(Particle e, double t);
void SetParticleID(Particle e,unsigned long id);
void SetParticleWeight(Particle e,double w);
ParticleKindT GetParticleKind(Particle e);
void GetParticleCoordinates(Particle e,double *x,double *y,double *z);
void GetParticleMomentum(Particle e,double *px,double *py,double *pz);
double GetParticleTime(Particle e);
double GetParticleSpeed(Particle e);
double GetParticleEnergy(Particle e);
double GetParticleWeight(Particle e);
double GetTimeRefinement(void);
void MultiplyWeight(Particle e,double coef);
double randE(double min,double max);

/* Initialization */
void InitParticleClass(World w);
/* Step */
void DepositAllEnergy(Particle e);
ParticleStatusT GeneralParticleStep(Particle particle,Particle newParticle,double tfin);
/* Time step calculations */
double GetMinimumElectronDtWithGivenField(double Efield,double B,double Nm);
double GetMinimumDtUniform(void);
double GetMinimumDtUniform1(void);
double GetSingleElectronDt(Particle electron);
/* Output */
void PrintParticleInfo(void *particle);
void DumpParticleLine(Particle particle,FILE *file);
void GetRandomlyDistributedParticle(Particle particle, double thetamax,
		double angle, double emin, double emax, int kind,
		double weight, double x, double y, double z);
void ReadParticleLineFromConsole(Particle particle);
Particle ReadParticleLineFromFile(FILE *file, Particle e);

#define PENETRD 1

#if PENETRD
double GetZmin(void);
#endif

#endif /* _particle_h_ */
