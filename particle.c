/*
 * File: particle.c
 * ----------------
 * This file contains (almost) only physics. It contains the ideology of the particle
 * propagation implementation. All actual cross-sections are in "crossec.c".
 *
 * History of changes:
 * 10/4/99
 * Added photon propagation. Units of physical values are the same as for electrons.
 * It is implemented only for stack or optimized null collision method (no calculation
 * of common dt).
 * Each particle has some effective weight, because it represents many particles.
 * We assume that all energy after Compton scattering is deposited within 1 km.
 * [Brown, 1973]
 * The processes included so far are incoherent Compton scattering and photoeffect.
 * The program is finished when all photons are gone (the photon is
 * removed and its energy deposited, when its weight is reduced below Wmin) or
 * they are backscattered.
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "interface.h"
#include "util.h"
#include "particle.h"
#include "world.h"
#include "crossec.h"

/* options */
/* MIRROR -- include the magnetic mirroring effect */
#define MIRROR             1
#if MIRROR
#define MIRROR_Z_ONLY      1
#endif

#define PRINT_DT           0
#define USE_DIFFUSION_DT   1
/* setting IONIZATION_TIME to 0 makes the calculation a bit faster */
#define IONIZATION_TIME    0

static const double LIGHT_SPEED=299792.; /* in km/sec */

struct ParticleCDT{
	ParticleKindT k;   /* kind - electron or photon */
	double w;          /* weight */
	double px,py,pz;   /* momentum */
	double v,p,g;      /* velocity, momentum and total energy */
	                   /* for a photon, momentum is equal to energy, velocity is always 1 */
	double x,y,z;      /* coordinates in km */
	double t;          /* time in seconds */
	long id;           /* particle number */
};

/****** STATIC FUNCTION DECLARATIONS *******/
static void InitElectronClass(World w);
static void InitPhotonClass(void);
static ParticleStatusT ElectronStep(Particle e, double dt, Particle enew,int createOption);
static ParticleStatusT StepPhoton(Particle photon,Particle newElectron,double tfin);
static ParticleStatusT StepElectron(Particle electron,Particle newParticle,double tfin);
static ParticleStatusT StepPositron(Particle positron,Particle newParticle,double tfin);
static void Update(Particle e);
static double FdCorrected(double p);
static double GetDynamicDt(Particle electron);
static double GetDtWithGivenField(bool minimum,bool ionization,double p,double Efield,double B,double Nm);
static double GetDt(Particle e,bool ionization);
static void CalculateComptonScattering(Particle e,Particle newElectron);
static void CalculatePairProductionAnnihilation(Particle photon,Particle photon2);
static void randPxPyPz(double e, double thetamax, double* px, double* py, double* pz);
#if PENETRD
double GetZmin(void);
#endif

/****** STATIC (GENERAL FOR THE CLASS) VARIABLES ******/
static World TheWorld;
//static Simulation TheSimulation;
static double TimeRefinement;  /* time step multiplier */
static double TheEminElec;  /* minimum electron energy */
static double TheEminPhot;  /* minimum photon energy */
/* Preferences for electron trajectories */
static struct {
	bool fric,diff,ioniz,create,simpleRandomEnergy,simpleCollisionRate,bremsstrahlung;
	bool dynamicTimeStep; /* if TRUE, a more precise (?) way to calculate ionization */
	ElectronScatteringT scatteringType;
} PREF;

#if MIRROR
#if MIRROR_Z_ONLY
static double TheGradB;
#else
static double TheGradBx,TheGradBy,TheGradBz,TheGradB;
#endif
#endif

#if PENETRD
	double ZMIN=-1;
#endif

#if PENETRD
/**** minimum penetration depth */
double GetZmin(void)
{
	return ZMIN;
}
#endif

/*********** ACCESS TO CLASS VARIABLES ***********/

// Maybe better to return void* and typecast to needed?
bool GetParticleClassProperty(char *property)
{
	if (strcmp(property,"CREATE")==0)
		return PREF.create;
	else if (strcmp(property,"FRICTION")==0)
		return PREF.fric;
	else Error("Unknown property: %s",property);
	/* Should never get here, but quench compiler complaints */
	return FALSE;
}

/*********** PUBLIC MEMBER FUNCTIONS ***********/

size_t ParticleMemUsage(void)
{
	return sizeof(struct ParticleCDT);
}

void SetParticleKind(Particle e,ParticleKindT kind)
{
	e->k=kind;
}

void SetParticleMomentum(Particle e, double px, double py, double pz)
{
	e->px=px; e->py=py; e->pz=pz;
	Update(e);
}

void SetParticleCoordinates(Particle e, double x, double y, double z)
{
	e->x=x; e->y=y; e->z=z;
}

void SetParticleTime(Particle e, double t)
{
	e->t=t;
}

void SetParticleID(Particle e,unsigned long id)
{
	e->id=id;
}

void SetParticleWeight(Particle e,double w)
{
	e->w=w;
}

ParticleKindT GetParticleKind(Particle e)
{
	return e->k;
}

void GetParticleCoordinates(Particle e,double *x,double *y,double *z)
{
	*x=e->x; *y=e->y; *z=e->z;
}

void GetParticleMomentum(Particle e,double *px,double *py,double *pz)
{
	*px=e->px; *py=e->py; *pz=e->pz;
}

double GetParticleTime(Particle e)
{
	return e->t;
}

double GetParticleSpeed(Particle e)
{
	return LIGHT_SPEED*(e->v);
}

double GetParticleEnergy(Particle e)
{
	switch(e->k){
	case ELECTRONKIND:
	case POSITRONKIND:
		return e->g-1;
		break;
	case PHOTONKIND:
		return e->p;
		break;
	default:
		Error("GetParticleEnergy: unknown particle kind");
	}
	return 0; /* we never get here */
}

double GetParticleWeight(Particle e)
{
	return e->w;
}

double GetTimeRefinement(void)
{
	return TimeRefinement;
}

// addition

//returns random energy in range min...max distributed as 1/E.
double randE(double min,double max){
	double X;
	double randomE;
	X=MyRandom();
	randomE=min*pow(max/min,X);
	if(isnan(randomE))
		Error("isnan(randomE), X=%lg",X);
    return randomE;
}

//sets px,py,pz randomly in solid angle appropriately for the given energy.
static void randPxPyPz(double p, double thetamax, double* px, double* py, double* pz){
    double phi,costh,sinth;
    phi = 2*PI*MyRandom();
    if(fabs(thetamax)>1e-6)
    	costh=MyRandom()*(1-cos(thetamax))+cos(thetamax);
	else
		costh=1;
    if(fabs(costh)>1)
    	Error("randPxPyPz: costh > 1");
    sinth=sqrt(1-costh*costh);
    *px = p*sinth*cos(phi);
    *py = p*sinth*sin(phi);
    *pz = p*costh;
}



void MultiplyWeight(Particle e,double coef)
{
	e->w*=coef;
}

void InitParticleClass(World w)
{
	GetNumbers("<PARTICLE>","");
	InitElectronClass(w);
	InitPhotonClass();
	GetNumbers("</PARTICLE>","");
}

static void InitElectronClass(World w)
{
	int sc;
	TheWorld=w;
	GetNumbers("Refinement coefficient (0<alfa<1):","f",&TimeRefinement);
	GetNumbers("Electron minimum energy (keV)?","f",&TheEminElec);
	TheEminElec/=511.; /* convert to dimensionless */
	/* Preferences */
	GetNumbers("Friction (y/n):","b",&PREF.fric);
	GetNumbers("Diffusion (y/n):","b",&PREF.diff);
	GetNumbers("Ionization (y/n):","b",&PREF.ioniz);
	GetNumbers("Creation of new electrons (y/n):","b",&PREF.create);
	GetNumbers("Bremsstrahlung (y/n):","b",&PREF.bremsstrahlung);
	GetNumbers("Scattering type: 1-regular, 2-right-angle, 3-forward:",
		"d",&sc);
	switch(sc){
	case 1:
		PREF.scatteringType=REGULAR;
		break;
	case 2:
		PREF.scatteringType=RIGHT_ANGLE;
		break;
	case 3:
		PREF.scatteringType=FORWARD;
		break;
	default:
		Error("Unknown scattering type");
	}
	GetNumbers("Simple calculation of random energy (y/n):",
		"b",&PREF.simpleRandomEnergy);
	GetNumbers("Simple calculation of collision rate (y/n):",
		"b",&PREF.simpleCollisionRate);
	GetNumbers("Dynamic dt calculation?","b",&PREF.dynamicTimeStep);

#if MIRROR
#if MIRROR_Z_ONLY
	GetNumbers("The vertical (grad B)/(2B) (1/km):","f",&TheGradB);
	TheGradB*=LIGHT_SPEED; /* convert to 1/sec */
#else
	GetNumbers("The (grad B)/(2B) components (1/km):","fff",&TheGradBx,&TheGradBy,&TheGradBz);
	TheGradBx*=LIGHT_SPEED; /* convert to 1/sec */
	TheGradBy*=LIGHT_SPEED;
	TheGradBz*=LIGHT_SPEED;
	TheGradB=sqrt(TheGradBx*TheGradBx+TheGradBy*TheGradBy+TheGradBz*TheGradBz);
#endif
#endif
}

static void InitPhotonClass(void)
{
	GetNumbers("Photon minimum energy (keV)?","f",&TheEminPhot);
	TheEminPhot/=511.; /* convert to dimensionless */
}

void DepositAllEnergy(Particle e)
{
#if DEBUG
	Printf("DepositAllEnergy(%lf)\n",(e->w)*GetParticleEnergy(e));
#endif	
	DepositEnergy(TheWorld,(e->w)*GetParticleEnergy(e),e->z,e->z);
}

/*
 * ElectronStep
 * ------------
 * createOption=0  --  usual null-collision method
 * createOption=1  --  the new particle must be created
 * createOption=2  --  the new particle must not be created.
 */
static ParticleStatusT ElectronStep(Particle e, double dt, Particle enew,int createOption)
{
	double Ex,Ey,Ez,Bx,By,Bz,B,Nm;
	double coef,p1x,p1y,p1z,phi;
	double vcoef,px,py,pz,p,p2,g,x;
	double tau,ca,sa,x0,y0,z0,gmin;
	double probability;
	double endep=0; /* deposited energy */
	/*double a0,a1,a2,a3,a4,a5;*/
	/*double inen;*/
#if MIRROR
#if MIRROR_Z_ONLY
	double pperp2;
#endif
#endif
	
#if PRINT_DT
		Printf("ElectronStep: dt=%g\n",dt);
#endif
	/*Printf("In the beginning:\n");
	PrintElectronInfo(e);*/
	/* Find the fields and density */
	if(UseE(TheWorld)){
		GetE(TheWorld,e->x,e->y,e->z,&Ex,&Ey,&Ez);
		Ex *= dt/2;   Ey *= dt/2;   Ez *= dt/2; /* now E is dimensionless */
	}
	GetB(TheWorld,&Bx,&By,&Bz);
	B=GetBabs(TheWorld);
	Nm=GetNm(TheWorld,e->z);
	gmin=1+TheEminElec;
	
	/* For debugging */
	/*inen=e->g-1;*/
	
	/* Make a copy of the particle information */
	px=e->px; py=e->py; pz=e->pz; p=e->p; g=e->g;
		
	/**********************************/
	/* Half advance in electric field */
	/**********************************/
	if(UseE(TheWorld)){
		px -= Ex;   py -= Ey;   pz -= Ez;
		p2=px*px+py*py+pz*pz;
		p=sqrt(p2); g=sqrt(p2+1);
		if(g < gmin) return DISAPPEAR; /* thermalize */
	}	

	/*****************/
	/* Half friction */
	/*****************/
	if(PREF.fric){
		coef=(1-(Nm*FdCorrected(p)/p)*(dt/2));
		px*=coef;  py*=coef;  pz*=coef; p*=coef;
		/* The energy deposition */
		endep += g;
		g=sqrt(p*p+1);
		endep -= g;
		if(g < gmin) return DISAPPEAR; /* thermalize */
	}
		

	/*********************/
	/* Angular Diffusion */
	/*********************/
	/* Random "kick" to simulate diffusion */
	/* Change on 10/21/98: substitute the solution of angular diffusion equation */
	/* The p and g do not change during diffusion */
	if(PREF.diff){
		/* Calculate the scattering angle */
		tau=0.25*dt*Nm*Theta2Growth(p); /* =D(p) \Delta t */
		/* 2. Cosine and sine of the polar angle */
		if(tau<.0001){
			/* Extremely small angle change: 4*tau = theta^2 */
			sa=sqrt(4*tau);
			ca=sqrt(1-sa*sa);
		}else{
			/* Large angle change */
			ca=DiffusionMu(tau);
			sa=sqrt(1-ca*ca);
		}
#if PRINT_DT
		Printf("Diffusion: ca=%g, sa=%g for\n",ca,sa);
		PrintParticleInfo(e);
#endif
		/* Save old value of the momentum */
		p1x=px; p1y=py; p1z=pz;
		/* The new momentum components in the system where old momentum is along z */
		phi=2*PI*MyRandom(); /* random azimuthal angle */
		px=p*sa*cos(phi);  py=p*sa*sin(phi);  pz=p*ca;
		ConvertToLabFrame(p1x,p1y,p1z,&px,&py,&pz);
	}
	
	/******************************/
	/* Rotation in Magnetic Field */
	/******************************/
	if(B>1e-6){
#if PRINT_DT
		Printf("mag field\n");
#endif
		coef=dt/g;
		/* Change the momenta */
		Rotate(&px,&py,&pz,Bx*coef,By*coef,Bz*coef);
		/* p and g don't change */	
	}


#if MIRROR
	/******************************/
	/* The magnetic mirror effect */
	/******************************/
	#if MIRROR_Z_ONLY
		#if PRINT_DT
			Printf("mag mirror\n");
			Printf("p={%g,%g,%g};   ",px,py,pz);
		#endif
		pperp2=px*px+py*py;
		if(pperp2/p/p > 1e-10){
			pz -= pperp2/g*TheGradB*dt;
			/* The perpendicular component is calculated from the requirement that
			   p^2 doesn't change */
			coef=sqrt((p*p-pz*pz)/pperp2);
			px*=coef; py*=coef;
		}
		#if PRINT_DT
			Printf("p'={%g,%g,%g}\n",px,py,pz);
		#endif
	#else
		ppar=(px*TheGradBx+py*TheGradBy+pz*TheGradBz)/TheGradB;
		pperp=sqrt(p*p-ppar*ppar);
		Not finished yet.
	#endif
#endif

	/*****************/
	/* Half friction */
	/*****************/
	if(PREF.fric){
		coef=(1-(Nm*FdCorrected(p)/p)*(dt/2));
		px*=coef;  py*=coef;  pz*=coef; p*=coef;
		/* The energy deposition */
		endep += g;
		g=sqrt(p*p+1);
		endep -= g;
		if(g < gmin) return DISAPPEAR; /* thermalize */
	}
	

	/**********************************/
	/* Half advance in electric field */
	/**********************************/
	if(UseE(TheWorld)){
		px -= Ex;   py -= Ey;   pz -= Ez;
		p2=px*px+py*py+pz*pz;
		p=sqrt(p2); g=sqrt(p2+1);
		if(g < gmin) return DISAPPEAR; /* thermalize */
	}

	/**********************************/
	/* Advance the spatial coordinate */
	/**********************************/
	vcoef=1/(e->g + g)*dt*LIGHT_SPEED;
	x0=e->x; y0=e->y; z0=e->z;
	e->x += (px + e->px)*vcoef;   e->y += (py + e->py)*vcoef;   e->z += (pz + e->pz)*vcoef;
#if PRINT_DT
	Printf("Spatial advance: p={%g,%g,%g}\n",px,py,pz);
	Printf("r={%g,%g,%g}, r'={%g,%g,%g}\n",x0,y0,z0,e->x,e->y,e->z); getchar();
#endif

	/* For debugging */
	/* Printf("Energy: ini=%g, final=%g, lost=%g, ini-final=%g, i-f-lost=%g\n",
		inen,g-1,endep,inen-g+1,inen-g+1-endep); */
	/* getchar(); */
	
	/* In the case of a non-uniform world, check if the particle flew away */
	if(!IsInside(TheWorld,e->x,e->y,e->z)) return LEAVE;
	
	if(PREF.fric && !IsUniform(TheWorld)){
		/* Deposit the energy - give the energy amount, the initial and final spatial location */
		DepositEnergy(TheWorld,e->w*endep,z0,e->z);
	}
	
	/* Advance momentum */
	e->px=px; e->py=py; e->pz=pz;
	Update(e);
	
	/* Advance the time */
	e->t += dt;
	
	/*Printf("In the end:\n");
	PrintElectronInfo(e);*/

	/* Removal of the electron due to thermalization */
	if(e->g < gmin)
		Error("ElectronStep: Failed to detect DIE, e->g=%g, g=%g",e->g, sqrt(p*p+1));

	/* Creation of a new electron */
	if(createOption==2) return NOTHING;
	if(e->g > 2*TheEminElec+1){
		probability=dt*Nm*CollisionRate(e->g,TheEminElec,PREF.simpleCollisionRate);
		/*Printf("probability=%g\n",probability);*/
		x=MyRandom();
		/*Printf("MyRandom=%g\n",x);*/
		if(createOption==1 || x < probability){
			/*Printf("createOption=%d\n",createOption);*/
			MollerIonization(&(e->px),&(e->py),&(e->pz),
				&(enew->px),&(enew->py),&(enew->pz),
				TheEminElec,PREF.scatteringType,PREF.simpleRandomEnergy);
			enew->w = e->w; enew->t = e->t;
			enew->x = e->x; enew->y = e->y; enew->z = e->z;
			Update(enew);
			Update(e);
			if(PREF.create) return DOUBLE;
		}
	}
	
	return NOTHING;
}

/*
 * StepPhoton
 * ----------
 * On 10/05/99, we still do not consider what happens to the Compton electron.
 * On 12/10/99, we take into account that there is time limit tfin and the boundary of the world.
 */
static ParticleStatusT StepPhoton(Particle photon,Particle newParticle,double tfin)
{
	double p0x,p0y,p0z,p0,x0,y0,z0,v0x,v0y,v0z;
	double photoProb,comptonProb,pairProb,x,thick,tac,cost,tacfin,distance,z1;
	Particle e=photon;
	ParticleStatusT statusExpLeave;
	double distExpire,distLeave,distExpLeave,distThick;
	
	/*Printf("StepPhoton(tfin=%g), id=%ld, p=%g={%g,%g,%g}, r={%g,%g,%g}\n",
		tfin,e->id,e->p,e->px,e->py,e->pz,e->x,e->y,e->z);*/
	p0x=e->px; p0y=e->py; p0z=e->pz;
	p0=e->p;
	x0=e->x; y0=e->y; z0=e->z;
	v0x=p0x/p0; v0y=p0y/p0; v0z=p0z/p0; 
	if(fabs(sqrt(v0x*v0x+v0y*v0y+v0z*v0z)-1)>1e-6) Error("v!=c");
	/* 1. Calculate when will be the next collision */
	/* Use the "cumulative probability" method to find the TAC between
	   collisions */
	/* The collision probabilities per unit time at NmConv */
	photoProb=PhotoProb(p0);
	comptonProb=ComptonProb(p0);
	pairProb=PairProb(p0);
	/* The atmosphere thickness between collisions */
	while((x=MyRandom())==0) { }
	thick = -LIGHT_SPEED/(photoProb+comptonProb+pairProb)*log(x);
	tac=GetTACfromZ(TheWorld,z0);
	cost=p0z/p0; /* cosine of the pitch angle */
	tacfin=tac-thick*cost; /* the TAC at the final photon position */
	/*Printf("tacfin=%g\n",tacfin);*/
	
	/* Find distance for photon expiring and flying away and determine which is shorter */
	distExpire=(tfin - e->t)*LIGHT_SPEED;
	if(BoundaryIntersect(TheWorld,x0,y0,z0,v0x,v0y,v0z,&distLeave)){
		/* note: no LIGHT_SPEED multiplication for distLeave */
		if(distLeave > distExpire){
			statusExpLeave=EXPIRE;
			distExpLeave=distExpire;
		}else{
			statusExpLeave=LEAVE;
			distExpLeave=distLeave;
		}
	}else{
		/* takes infinite time to leave */
		distExpLeave=distExpire;
		statusExpLeave=EXPIRE;
	}

	if(tacfin < 0 || tacfin>GetMaxTAC(TheWorld)){/* Photon flies away */
		e->x += v0x*distExpLeave;
		e->y += v0y*distExpLeave;
		e->z += v0z*distExpLeave;
		e->t += distExpLeave/LIGHT_SPEED;
		/*Printf("f");*/
		return statusExpLeave;
	}
	
	/* Photon does not fly away */
	/* Determine distance */
	if(fabs(cost)<1e-6){
		distThick=thick/GetNm(TheWorld,z0);
	}else{
		z1=GetZfromTAC(TheWorld,tacfin);
		distThick=(z1-z0)/(p0z/p0);
	}
	
	/* Determine final coordinates */
	distance=distThick > distExpLeave ? distExpLeave : distThick;
	e->x += distance*v0x;
	e->y += distance*v0y;
	e->z += distance*v0z;
	e->t += distance/LIGHT_SPEED;
	if(distThick > distExpLeave){
		/*Printf("e");*/
		/* No further evolution */
		/*if(statusExpLeave==LEAVE){
			Printf("StepPhoton(tfin=%g), id=%ld, p=%g={%g,%g,%g}, r={%g,%g,%g}\n",
				tfin,e->id,e->p,e->px,e->py,e->pz,e->x,e->y,e->z);
			Printf("was at r={%g,%g,%g}, d=%g (dEL=%g, dT=%g)\n",
				x0,y0,z0,distance,distExpLeave,distThick); getchar();
		}*/
		return statusExpLeave;
	}
	
	/*Printf("s");*/
	/*Printf("Staying: t=%g, p=%g={%g,%g,%g}, r={%g,%g,%g}\n",
		e->t,e->p,e->px,e->py,e->pz,e->x,e->y,e->z);*/
	if(e->t >= tfin) Error("EXPIRE");
	
	
	/* 2. Compton Scattering or Photoeffect? */
	x=MyRandom()*(photoProb+comptonProb+pairProb);
	if(x < photoProb){
		/* Photoeffect */
		// The energy is NOT deposited in this function!
		return DISAPPEAR;
	}else if(x < photoProb+comptonProb){
		/* Compton Scattering */
		CalculateComptonScattering(e,newParticle);
		if(e->p < TheEminPhot){
			/* photon energy is too low */
			// The energy is NOT deposited in this function!
			return DISAPPEAR;
		}else return NOTHING;
	}else{
		/* Pair production -- simplified approach: combine it with annihilation */
		CalculatePairProductionAnnihilation(e,newParticle);
		// The energy is deposited in CalculatePairProductionAnnihilation
		return DOUBLE;
	}
	return NOTHING;
}

/*
 * CalculatePairProductionAnnihilation
 * -----------------------------------
 * Assume that both the electron and positron slow down fast, because
 * the positron annihilation is largest at small positron energy.
 * From conservation laws it then follows that 2 photons
 * of energy 2mc^2 are emitted.
 * Their direction is isotropic and opposite.
 */
static void CalculatePairProductionAnnihilation(Particle photon,Particle photon2)
{
	double phi,px,py,pz,pp,p;
	// Printf("CalculatePairProductionAnnihilation\n");
	p=photon->p;
	if(p<=2) Error("Not enough energy for pair creation");
	phi=2*PI*MyRandom();
	pz=2*MyRandom()-1;
	pp=sqrt(1-pz*pz);
	px=pp*cos(phi);
	py=pp*sin(phi);
	SetParticleKind(photon2,PHOTONKIND);
	photon2->w=photon->w;
	photon2->t=photon->t;
	SetParticleCoordinates(photon2,photon->x,photon->y,photon->z);
	SetParticleMomentum(photon,px,py,pz);
	SetParticleMomentum(photon2,-px,-py,-pz);
	DepositEnergy(TheWorld,photon->w*(p-2),photon->z,photon->z);
}

/*
 * CalculateComptonScattering
 * --------------------------
 * Change the photon momentum and energy in a random way according
 * to Compton scattering process.
 * Change on 3/28/2005: don't discard the electron. 
 */
static void CalculateComptonScattering(Particle e,Particle newElectron)
{
	double px,py,pz,p,p1,ca,sa,cp,sp,phi;
#if 0
	double energy,pe,cae,sae;
#endif
	
	/* Save old photon momentum */
	px=e->px;
	py=e->py;
	pz=e->pz;
	p=e->p;
	/* Energy after collision */
	p1=RandomComptonEnergy(p,MyRandom());
#if DEBUG
	//Printf("Compton electron Deposit %lf\n",energy);
#endif	
	//DepositEnergy(TheWorld,energy,e->z,e->z);
	/* Polar angle */
	ca=ComptonCosAngle(p,p1);
	sa=sqrt(1-ca*ca);
	/* Azimuthal angle */
	phi=2*PI*MyRandom();
	cp=cos(phi);
	sp=sin(phi);
	/* The photon momentum in the frame of primary */
	e->px=p1*sa*cp;
	e->py=p1*sa*sp;
	e->pz=p1*ca;
	/* Rotate it back to the original frame [Fano et al, p. 774] */
	ConvertToLabFrame(px,py,pz,&(e->px),&(e->py),&(e->pz));
	e->p=p1;
	e->g=p1;
#if 0
	/* Electron */
	pe=sqrt((p-p1+1)*(p-p1+1)-1);
	cae=(p-p1)*(1+1/p)/pe;
	sae=sqrt(1-cae*cae);
	newElectron->k=ELECTRONKIND;
	SetParticleMomentum(newElectron,-pe*sae*cp,-pe*sae*sp,pe*cae);
	SetParticleCoordinates(newElectron,e->x,e->y,e->z);
	SetParticleTime(newElectron,e->t);
	SetParticleWeight(newElectron,e->w);
	Update(newElectron);
	ConvertToLabFrame(px,py,pz,&(newElectron->px),&(newElectron->py),&(newElectron->pz));
#else
	DepositEnergy(TheWorld,e->w*(p-p1),e->z,e->z);
#endif
}


/*********************** STEP CALCULATIONS ************************/

/*
 * GeneralParticleStep
 * -------------------
 * The time step is either given (dt>0) or not (dt<0).
 * For electrons, we use the option of the dynamic time step (PREF.dynamicTimeStep).
 * This means that the ionization process is not taken
 * into account when calculating the time step, but
 * the ionization (calculated with GetDynamicDt) can occur
 * inside the time step at a random moment of time.
 * The time step is then reduced to appropriate value.
 * The time step for photon is ignored.
 */
ParticleStatusT GeneralParticleStep(Particle particle,Particle newParticle,double tfin)
{
#if PENETRD
	if(ZMIN<0 || particle->z < ZMIN) ZMIN=particle->z;
#endif
	switch(GetParticleKind(particle)){
	case ELECTRONKIND:
		return StepElectron(particle,newParticle,tfin);
	case PHOTONKIND:
		return StepPhoton(particle,newParticle,tfin);
	case POSITRONKIND:
		return StepPositron(particle,newParticle,tfin);
	default:
		Error("GeneralParticleStep: unknown particle kind");
	}
	/* Never get here */
	Error("GeneralParticleStep:  should never get here");
	return NOTHING;
}

static ParticleStatusT StepPositron(Particle positron,Particle newParticle,double tfin)
{
	Error("StepPositron: not implemented");
	return NOTHING;
}

/*
 * StepElectron
 * ------------
 * We need, beside doing the step itself, calculate the time interval if it
 * is not given.
 * We have to know the final time tfin to calculate whether the particle has expired.
 * When tfin<0, we do not compare particle time to expiration time.
 */
static ParticleStatusT StepElectron(Particle electron,Particle newParticle,double tfin)
{
	bool unload=FALSE;
	int createOption;
	double dtdyn,dtfin;
	ParticleStatusT status;
	double dt;
	
	/*Printf(">>>>>Step for\n");
	PrintParticleInfo(electron); getchar();*/

	/* Printf("getting dt ..."); getchar(); */
	dt=TimeRefinement*GetDt(electron,!PREF.dynamicTimeStep);
	/* include ionization time only if not using dynamic dt */
		/*Printf("dt=%g, v=%g, h=%g\n",
	dt,GetParticleSpeed(electron),GetScaleHeight(s->world)); getchar();*/
	if(!IsUniform(TheWorld) && dt*GetParticleSpeed(electron) > GetScaleHeight(TheWorld)){
			/*Printf("Step: particle flies away, dt=%g, v=%g, h=%g\n",
		dt,GetParticleSpeed(electron),GetScaleHeight(s->world));*/
		dt=GetScaleHeight(TheWorld)/GetParticleSpeed(electron);
	}

	/*** Choosing the correct dt, "unload" and the createOption ***/
	if(tfin>0){
		if(GetParticleTime(electron) > tfin) Error("in Step");
		/* Unload */
		dtfin = tfin - GetParticleTime(electron);
		if(dtfin < dt){
			/* Prepare to unload */
			unload=TRUE;
			dt=dtfin;
		}/* if dt < dtfin, unload remains FALSE */
	}
	
	/* Dynamic -- disabled for our old fiend null-collision method */
	if(PREF.dynamicTimeStep){
		/* Calculate the next ionization time */
		dtdyn=GetDynamicDt(electron);
		if(dt > dtdyn){
			/*Printf("creating: dt=%g, dtdyn=%g",dt,dtdyn); getchar();*/
			/* we must use the dynamic dt and create a new particle */
			createOption=1;
			unload=FALSE;
			dt=dtdyn;
		}else{
			/* we must use the dt calculated above and NOT create a new particle */
			/*Printf("not creating: dt=%g, dtdyn=%g",dt,dtdyn); getchar();*/
			createOption=2;
		}
	}else createOption=0;


	/* createOption=0  --  usual null-collision method (probability of creation = rate*dt)
	 * createOption=1  --  the new particle must be created
	 * createOption=2  --  the new particle must not be created. */
	status=ElectronStep(electron,dt,newParticle,createOption);
	/*Printf("status = %d\n",(int)status);*/
	if(unload){
		/*Printf("Unloading .. \n");*/
		switch(status){
		case NOTHING:
			return EXPIRE;
		case LEAVE:
			return LEAVE;
		case DISAPPEAR:
			return DISAPPEAR;
		case DOUBLE:
			/* We neglect the newly created particle if it is immediately expired */
			return EXPIRE;
		default:
			Error("Unknown option");
		}
	}else return status;
	Error("StepElectron: Never get here");
	return NOTHING;
}

/************************************ TIME STEP *************************************/

/*
 * GetMinimumElectronDtWithGivenField, GetMinimumDtUniform, GetSingleElectronDt
 * ----------------------------------------------------------------------------
 * Public interfaces for time interval calculations.
 */
double GetMinimumElectronDtWithGivenField(double Efield,double B,double Nm)
{
	return GetDtWithGivenField(TRUE,TRUE,0,Efield,B,Nm);
}

/*
 * GetMinimumDtUniform
 * -------------------
 * Calculate next time step on the basis of the electron energies and
 * field strength (electric and magnetic).
 * Faster version, called only once in the beginning.
 * It calculates the smallest dt, relevant for an electron of lowest energy.
 */
double GetMinimumDtUniform(void)
{
	if(!IsUniform(TheWorld))
		Error("GetMinimumDtUniform1: called for nonuniform");
	return GetDtWithGivenField(TRUE,TRUE,0,GetEabs(TheWorld,0,0,0),GetBabs(TheWorld),1.);
}

double GetMinimumDtUniform1(void)
{
	double dt,g,pmin,pmax,lpmin,lpmax,dp,lp,p,dt1,Eabs,Babs;
	int i,n;
	
	if(!IsUniform(TheWorld))
		Error("GetMinimumDtUniform1: called for nonuniform");
	dt=1.;
	n=100;
	g=TheEminElec+1;
	pmin=sqrt(g*g-1);
	pmax=100;
	lpmin=log(pmin);
	lpmax=log(pmax);
	dp=(lpmax-lpmin)/(n-1);
	Eabs=GetEabs(TheWorld,0,0,0);
	Babs=GetBabs(TheWorld);
	for(i=0,lp=lpmin; i<n; i++,lp+=dp){
		p=exp(lp);
		dt1=GetDtWithGivenField(FALSE,TRUE,p,Eabs,Babs,1.);
		/*Printf("GetMinimumDtUniform1: p=%g, dt=%g\n",p,dt1);*/
		if(dt1<dt) dt=dt1;
	}
	return dt;
}


double GetSingleElectronDt(Particle electron)
{
	return GetDt(electron,TRUE);  /* include ionization */
}

/* Private functions */

static double GetDynamicDt(Particle e)
{
	double rate,x;
	rate=GetNm(TheWorld,e->z)*CollisionRate(e->g,TheEminElec,PREF.simpleCollisionRate);
	while((x=MyRandom())<=0)
		; /* choose non-zero random number from [0,1] */
	return (-1/rate*log(x));
	
}

static double GetDtWithGivenField(bool minimum,bool ionization,double p,double Efield,double B,double Nm)
{
	double dt,probmax1,probmax2,probmax,dt1,fric;
#if USE_DIFFUSION_DT
	double diff;
#endif
#if MIRROR
	double grb;
#endif

#if PRINT_DT
	Printf("DT: min=%d, ion=%d, p=%g, E=%g, B=%g, Nm=%g\n",(int)minimum,(int)ionization,p,Efield,B,Nm);
#endif
	dt=1;
	/* pmin==sqrt(gmin*gmin-1)==sqrt(Emin*(2+Emin)) */
	if(minimum){
		/* We discard the given value of p */
		p=sqrt(TheEminElec*(2+TheEminElec));
	}
	
	/* 1. Magnetic field: dt must be < gyroperiod */
	if(B>1e-6){ /* avoid dividing by zero */
		dt1=1./B;
		if(dt>dt1) dt=dt1;
#if PRINT_DT
		Printf("dt_mag=%e  ",dt1);
#endif
	}

	
#if MIRROR
	/* n. Magnetic mirror effect */
#if MIRROR_Z_ONLY
	grb=TheGradB>0 ? TheGradB : -TheGradB;
#else
	grb=TheGradB;
#endif
	if(grb > 1e-6){
		dt1=1./grb;
		if(dt>dt1) dt=dt1;
#if PRINT_DT
		Printf("dt_mir=%e  ",dt1);
#endif
	}
#endif


	/* 2. Ionization: dt must be < collision period */
	if(IONIZATION_TIME || ionization){
		if(minimum){
			/* Find the maximum of the function CollisionRate(g)=Nm Zm v sigmatot --
			   see Mathematica file "main.nb", section "The probability of collision" */
			probmax1=Nm * 0.154319/pow(TheEminElec,1.5);
			probmax2=Nm * 2/TheEminElec; /* for g->inf */
			probmax = (probmax1 > probmax2) ? probmax1 : probmax2;
		}else{
			if(sqrt(p*p+1)>2*TheEminElec+1)
				probmax=Nm*CollisionRate(sqrt(p*p+1),TheEminElec,PREF.simpleCollisionRate);
			else probmax=0.5; /* just any number <1 */
		}
		/*Printf("probmax=%g\n",probmax);*/
		if(probmax > 1){
			dt1=1./probmax;
			if(dt>dt1) dt=dt1;
#if PRINT_DT
			Printf("dt_ion=%e  ",dt1);
#endif
		}
	} /* ionization */

	
	/* 3. Electric field: dt must be < time to double the momentum */
	if(Efield>1e-6){ /* avoid dividing by zero */
		dt1=p/Efield;
		if(dt>dt1) dt=dt1;
#if PRINT_DT
		Printf("dt_el=%e  ",dt1);
#endif
	}
	
#if USE_DIFFUSION_DT
    /* We removed constrain on dt due to diffusion time on 10/21/98 */
	/* 4. Diffusion: dt must be < time of significant change of momentum due to diffusion */
	if(PREF.diff && (diff=Nm*Theta2Growth(p)) > 1e-6){
		dt1=1/(diff*4); /* coefficient 1/2 is for fun */
		/* Printf("dtheta_max=%e ",sqrt(dt1*Theta2Growth(pmin))); */
		if(dt>dt1) dt=dt1;
#if PRINT_DT
		Printf("dt_dif=%e  ",dt1);
#endif
	}
#endif /* USE_DIFFUSION_DT */
	/* 5. Friction: dt must be < time of slowdown */
	if((fric=Nm*FdTot(p)) > 1e-6){
		dt1=p/fric;
		if(dt>dt1) dt=dt1;
#if PRINT_DT
		Printf("dt_col=%e  ",dt1);
#endif
	}
#if PRINT_DT
	Printf("dt=%e\n",dt); getchar();
#endif
	return dt;
}

/*
 * GetDt
 * -----
 * Calculate next time step on the basis of the electron energy and
 * field strength (electric and magnetic) at the point where it is located.
 * The boolean "ionization" means whether we should take the ionization
 * characteristic time into account.
 */
static double GetDt(Particle e,bool ionization)
{
	/*Printf("Getting dt for p=%g, E=%g, B=%g, Nm=%g\n",
		e->p,GetEabs(TheWorld,e->x,e->y,e->z),
		GetBabs(TheWorld),GetNm(TheWorld,e->z));*/
	return GetDtWithGivenField(FALSE,ionization,e->p,GetEabs(TheWorld,e->x,e->y,e->z),
		GetBabs(TheWorld),GetNm(TheWorld,e->z));
}

/*********************** END TIME STEP CALCULATIONS ************************/

/*
 * PrintElectronInfo
 * -----------------
 * For debugging.
 */
void PrintParticleInfo(void *particle)
{
	Particle e=(Particle)particle;
	switch(e->k){
	case ELECTRONKIND:
		Printf("elec ");
		break;
	case PHOTONKIND:
		Printf("phot ");
		break;
	case POSITRONKIND:
		Printf("posi ");
		break;
	default:
		Error("PrintParticleInfo: unknown particle kind");
	}
	Printf("%ld: w=%g, v=%g, g=%g, p=%g, t=%g\np={%g,%g,%g}, r={%g,%g,%g}\n",
		e->id,e->w,e->v,e->g,e->p,e->t,e->px,e->py,e->pz,e->x,e->y,e->z);
}

void DumpParticleLine(Particle particle,FILE *file)
{
	/* Kind */
	fprintf(file,"%1d\t",(int)(particle->k));
	/*switch(e->k){
	case ELECTRONKIND:
		fprintf(file,"0\t");
		break;
	case PHOTONKIND:
		fprintf(file,"1\t");
		break;
	default:
		Error("DumpParticleLine: unknown particle kind");
	}*/
	/* Weight */
	fprintf(file,"%14.5e\t",particle->w);
	/* Momentum components */
	fprintf(file,"%14.5e\t%14.5e\t%14.5e\t",
		particle->px,particle->py,particle->pz);
	/* Spatial coordinates */
	fprintf(file,"%14.5e\t%14.5e\t%14.5e\t",
		particle->x,particle->y,particle->z);
	/* The time */
	fprintf(file,"%14.5e\n",particle->t);
}

void GetRandomlyDistributedParticle(Particle particle, double thetamax,
		double angle, double emin, double emax, int kind,
		double weight, double x, double y, double z)
{
	double p,px,py,pz,e;
	double pxtemp;
	double pztemp;

	e=randE(emin,emax);
	if(kind!=0) e+=1;
	if(kind==0) p=e; else p=sqrt(e*e-1);
    randPxPyPz(p,thetamax,&px,&py,&pz);
	pxtemp=px;
	pztemp=pz;
    px = cos(angle)*pxtemp+sin(angle)*pztemp;
    pz = cos(angle)*pztemp-sin(angle)*pxtemp;

	particle->k=(ParticleKindT)kind;
	particle->w=weight;

	SetParticleMomentum(particle,px,py,pz); /* This also updates the v,p,g stuff */
	SetParticleCoordinates(particle,x,y,z);

	/* The next two are not important, but we want to avoid NaNs as particle parameters */
	particle->t=0;
	particle->id=0;
	// PrintParticleInfo(particle);
}


void ReadParticleLineFromConsole(Particle e)
{
	double weight,px,py,pz,x,y,z;
	int kind;
	GetNumbers("","dfffffff",&kind,&weight,&px,&py,&pz,&x,&y,&z);
	/* Set the particle parameters */
	e->k=(ParticleKindT)kind;
	e->w=weight;
	SetParticleMomentum(e,px,py,pz); /* This also updates the v,p,g stuff */
	SetParticleCoordinates(e,x,y,z);
	/* The next two are not important, but we want to avoid NaNs as particle parameters */
	e->t=0;
	e->id=0;
}

Particle ReadParticleLineFromFile(FILE *file, Particle e)
{
	double weight,px,py,pz,x,y,z,kind;
	if(fscanf(file,"%lf%lf%lf%lf%lf%lf%lf%lf",&kind,&weight,&px,&py,&pz,&x,&y,&z)!=8)
		return NULL;
	/* Set the particle parameters */
	e->k=(ParticleKindT)floor(kind+0.5);
	e->w=weight;
	SetParticleMomentum(e,px,py,pz); /* This also updates the v,p,g stuff */
	SetParticleCoordinates(e,x,y,z);
	/* The next two are not important, but we want to avoid NaNs as particle parameters */
	e->t=0;
	e->id=0;
	return e;
}

/*********** PRIVATE MEMBER FUNCTIONS ***********/

/*
 * Update
 * ------
 * Get the electron's total velocity, momentum and energy.
 */
static void Update(Particle e)
{
	double p2;
	switch(e->k){
	case ELECTRONKIND:
	case POSITRONKIND:
		p2 = e->px*e->px + e->py*e->py + e->pz*e->pz;
		e->g = sqrt(1+p2);
		e->p=sqrt(p2);
		e->v=sqrt(1 - 1./(1+p2));
		break;
	case PHOTONKIND:
		e->v=1;
		e->p=sqrt(e->px*e->px + e->py*e->py + e->pz*e->pz);
		e->g=e->p;
		break;
	default:
		Error("Update: unknown particle kind");
	}
}

/*
 * FdCorrected
 * -----------
 * Dinamic electron friction force in inverse seconds (real force / m*c).
 * corrected for ionization.
 */
static double FdCorrected(double p)
{
	double f;
	f=FdTot(p);
	if(PREF.ioniz) f-=FdIon(p,TheEminElec);
	if(f<0) Error("f<0 at energy=%g keV",(sqrt(p*p+1))-1*511);
	return f;
}

