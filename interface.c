/*
 * File: interface.c
 *
 * An interface between the exernal world and deep mysteries of
 * scientific calculation.
 * Needs "util.c", namely function "Open".
 * Contains the "main" function.
 *
 * History:
 *
 * 5/24/99
 * Logically united it with the "GetNumbers" function, which
 * was in "util.c" before. 
 *
 * 5/6/97
 * Created for use with project "em".
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "util.h"
#include "interface.h"

/* This is needed on a Macintosh to read the command line arguments */
#ifdef __MWERKS__
#include <console.h>
#endif

/* Variables for internal use */
static FILE *InputFile;
static FILE *OutputFile=NULL;
static bool fileClosed;

/*
 * main
 * ----
 * This serves as an interface to function Main
 */
int main(int argc, char **argv)
{
	int mainResult;
#ifdef __MWERKS__
	argc=ccommand(&argv);
#endif
	if(argc<=1){
		fileClosed=TRUE;
		printf("***INTERACTION***\n");
		InputFile=stdin;
	}else{
		fileClosed=FALSE;
		printf("***SCRIPT '%s'***\n",argv[1]);
		InputFile=Open(argv[1],"r");
	}
	if(argc>=3){
		OutputFile=Open(argv[2],"w");
		fprintf(OutputFile,"***SCRIPT '%s'***\n",argv[1]);
		Printf("***OUTPUT '%s'***\n",argv[2]);
	}
	/* Call Main */
	mainResult=Main();
	/* Clean up */
	StartCalculations();
	/* well, we are not really starting calculations --
	   but this function does exactly what we need */
	Printf("***END***\n");
	return mainResult;
}

int VPrintf(char *format, va_list args)
{
	int n;
	n=vprintf(format,args); fflush(stdout);
	if(OutputFile!=NULL){
		vfprintf(OutputFile,format,args); fflush(OutputFile);
	}
	return n;
}

int Printf(char *format, ...)
{
	int n;
	va_list args;
	va_start(args, format);
	n=VPrintf(format,args);
	va_end(args);
	return n;
}

/*
 * GetNumbers
 * ----------
 * Very useful when anticipating numeric input.
 * Provides flexibility regarding the source of input:
 * can be from console or from a file.
 * In the latter case the file should contain all the
 * promts, exactly corresponding to the prompt passed to
 * this subroutine.
 * The input stream is set once by calling InitInput()
 * and destroyed in the end by calling CleanupInput().
 * The type of data for input is given in "myFormat", in which each
 * character corresponds to a value:
 *  f  float/double ("%lg")
 *  d  integer ("%d")
 *  l  long integer ("%ld")
 *  s  string: the arguments corresponding to it are (int n,char *s),
 *         where n is the size of array s.
 */
void GetNumbers(char *prompt,char *myFormat, ...)
{
	static char buf[256];
	static void *addresses[256];
	static int stringLengths[256];
	int numArguments,numStrings,i,iString;
	va_list ap;
	char c,boolValue;
	/* bool *theBoolAddress; */

	if(InputFile==NULL)
		Error("GetNumbers: cannot get \"%s\" because the file is closed",prompt);
	/* Check for '\n' in prompt */
	i=0;
	while((c=prompt[i++])!='\0')
		if(c=='\n')
			Error("GetNumbers: multiple strings are not supported");

	/* Analyse myFormat */
	va_start(ap,myFormat);
	numArguments=0; numStrings=0;
	while(myFormat[numArguments]){
		if(myFormat[numArguments]=='s') /* reading a string, should know its length */
			stringLengths[numStrings++]=va_arg(ap,int)-1;
		addresses[numArguments++]=va_arg(ap,void*);
	}
	va_end(ap);

	if(prompt[0]!='\0'){ /* Non-empty string */
		Printf("%s\n",prompt);
		if(InputFile!=stdin){
			if(!fgets(buf,256,InputFile))
				Error("unexpected end of script file");
			/* Remove '\n' */
			i=strlen(buf); buf[i-1]='\0';
			if(strcmp(buf,prompt)){
				//Error("'%s''%s'",prompt,buf);
				Error("asked:'%s';\nreading:     '%s' (different data)",prompt,buf);
			}
		}
	}

	/* Perform the input */
	iString=0; /* string counter */
	for(i=0;i<numArguments;i++){
		switch(myFormat[i]){
		case 'f':
			if(fscanf(InputFile,"%lg",(double *)addresses[i])!=1)
				Error("float value expected");
			break;
		case 'd':
			if(fscanf(InputFile,"%d",(int *)addresses[i])!=1)
				Error("integer value expected");
			break;
		case 'l':
			if(fscanf(InputFile,"%ld",(long *)addresses[i])!=1)
				Error("long integer value expected");
			break;
		case 'b':
			while((c=getc(InputFile))==' ' || c=='\t' || c=='\n') {}
			if((boolValue=c)==EOF || (boolValue!='y' && boolValue!='n')){
				Printf("You entered: '%c'",boolValue);
				Error("boolean value expected");
			}
			/* theBoolAddress=(bool *)addresses[i]; */
			if(boolValue=='y')
				*(bool *)addresses[i]=TRUE;
			else
				*(bool *)addresses[i]=FALSE;
			break;
		case 's':
			if(fscanf(InputFile,"%s",buf)!=1)
				Error("string value expected");
			if(strlen(buf)>stringLengths[iString])
				Error("string is too long: %d > %d",(int)(strlen(buf)+1),stringLengths[iString]+1);
			strcpy((char *)addresses[i],buf);
			iString++;
			break;
		default:
			Error("unknown format: '%c'",myFormat[i]);
		}
	}
  
	/* Skip blanks */
	if(InputFile!=stdin){
		while((c=getc(InputFile))==' ' || c=='\t' || c=='\n') {}
		if(c!=EOF) ungetc(c,InputFile);
	}
  
	/* Echo output */
	if(InputFile!=stdin){
		for(i=0;i<numArguments;i++){
			switch(myFormat[i]){
			case 'f':
				Printf("  %g",*(double *)addresses[i]);
				break;
			case 'd':
				Printf("  %d",*(int *)addresses[i]);
				break;
			case 'l':
				Printf("  %ld",*(long int *)addresses[i]);
				break;
			case 'b':
				if(*(bool *)addresses[i])
					Printf("  YES");
				else
					Printf("  NO");
				break;
			case 's':
				Printf("  %s",(char *)addresses[i]);
				break;
			}
		}
		if(numArguments > 0) Printf("\n");
	}
	fflush(stdout);
}

/*
 * StartCalculations
 * -----------------
 * Close the input file when the calculations are started.
 * The call of this function is not obligatory.
 */
void StartCalculations(void)
{
	if(!fileClosed){
		/* Close the script file */
		Printf("***CLOSING THE INPUT FILE***\n");
		fclose(InputFile);
		InputFile=NULL;
	}
	fileClosed=TRUE;
}
