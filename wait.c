/*
 * File: wait.c
 * ------------
 */

#include "interface.h"
#include "wait.h"

static unsigned long TheCycle=0,TheLineLen=0;
static unsigned long counter,linecounter;

void InitWait(unsigned long cycle,unsigned long linelen)
{
	if(TheCycle!=0) Error("\"Wait\" can only initilized once");
	TheCycle=cycle;
	TheLineLen=linelen;
	ResetWait();
}

void ResetWait(void)
{
	counter=0;
	linecounter=0;
}

/*
 * Wait
 * ----
 * Output to console -- required on Mac to be able
 * to do anything else on the same computer
 */
bool Wait(void)
{
	/* printf("counter=%lu, linecounter=%lu",counter,linecounter);
	getchar(); */
	if(++counter==TheCycle){
		Printf("."); fflush(stdout);
		counter=0;
		/* Check whether we need to advance the line */
		if(++linecounter>=TheLineLen){
			Printf("\n"); fflush(stdout);
			linecounter=0;
			return TRUE;
		}
	}
	return FALSE;
}

