/*
 * File: fid.h
 * -----------
 * FID stands for "fill initial distribution"
 */

#ifndef _fid_h_
#define _fid_h_

#include "util.h"
#include "particle.h"

typedef struct FIDCDT *FIDT;

FIDT InitFID(void);
void ResetFID(FIDT fid, int iRun, bool verbose);
bool CopyNextFromFID(FIDT fid,Particle e);
int GetNumberOfInitialDistributions(FIDT fid);
/* For debugging */
void FIDContents(FIDT fid);

#endif /* _fid_h_ */
