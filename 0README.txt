This is a version of code to share with other people.

Usage:

make
./monte input.txt

Or just
./monte
and do the manual input

The supplied "input.txt" is for calculating probability of an electron to become a runaway, the initial electron momenta are in "einfo.dat", results are in "calculated_probability*.dat".
