/*
 * File: chain.c
 * -------------
 */

#include <string.h>
#include <stdlib.h>
#include "util.h"
#include "chain.h"

#define SIZE_OF_CHAIN_STRING 128

struct ChainCDT {
	char *s;
	int *n;
	int i;
};

Chain NewChain(const char *str)
{
	Chain c;
	if((c=malloc(sizeof(struct ChainCDT)))==NULL)
		Error("NewChain: not enough memory");
	if((c->s = malloc(SIZE_OF_CHAIN_STRING))==NULL)
		Error("NewChain: not enough memory");
	if((c->n = malloc(SIZE_OF_CHAIN_STRING*sizeof(int)))==NULL)
		Error("NewChain: not enough memory");
	c->i=0;
	if((c->n[0] = strlen(str)) >= SIZE_OF_CHAIN_STRING)
		Error("NewChain: the source string is too long");
	strcpy(c->s,str);
	return c;
}

const char *ChainString(Chain c)
{
	if(!c) Error("ChainString: a null chain");
	return c->s;
}

void AppendToChain(Chain c,const char *suf)
{
	if(!c) Error("AppendToChain: a null chain");
	/*Printf("AppendToChain: \"%s\"+\"%s\"\n",c->s,suf);*/
	if(strlen(suf) + strlen(c->s) >= SIZE_OF_CHAIN_STRING)
		Error("AppendToChain: chain becomes too long");
	strcat(c->s,suf);
	c->n[++c->i]=strlen(c->s);
	if(c->i >= SIZE_OF_CHAIN_STRING)
		Error("AppendToChain: too many additions");
}

void RestoreChain(Chain c)
{
	/*int i;*/
	if(!c) Error("RestoreChain: a null chain");
	if(c->i < 1) Error("Cannot restore the chain");
	/*for(i=0;i<=c->i;i++) Printf(" %d",c->n[i]);
	Printf("\nRestoreChain: before \"%s\",",c->s);*/
	c->s[c->n[--c->i]]='\0';
	/*Printf(" after: \"%s\"\n",c->s);*/
}

void AppendRestoreChain(Chain c,const char *suf,bool append)
{
	if(append)
		AppendToChain(c,suf);
	else
		RestoreChain(c);
}

void DestroyChain(Chain c)
{
	if(!c) Error("DestroyChain: a null chain");
	free(c->s);
	free(c->n);
	free(c);
}
