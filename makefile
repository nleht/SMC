CC=gcc -Wall -O4
OBJ=monte.o simulation.o interface.o mystack.o util.o world.o particle.o \
	crossec.o chain.o wait.o fid.o
all: monte
monte: $(OBJ)
	$(CC) -o monte $(OBJ) -lm
monte.o: monte.c util.h wait.h simulation.h interface.h
	$(CC) -c monte.c
simulation.o: simulation.c simulation.h interface.h util.h world.h \
        particle.h mystack.h fid.h wait.h chain.h
	$(CC) -c simulation.c
interface.o: interface.c interface.h
	$(CC) -c interface.c
mystack.o: mystack.c mystack.h
	$(CC) -c mystack.c
util.o: util.c util.h
	$(CC) -c util.c
world.o: world.c world.h
	$(CC) -c world.c
particle.o: particle.c particle.h
	$(CC) -c particle.c
crossec.o: crossec.c crossec.h
	$(CC) -c crossec.c
chain.o: chain.c chain.h
	$(CC) -c chain.c
wait.o: wait.c wait.h
	$(CC) -c wait.c
fid.o: fid.c fid.h interface.h util.h particle.h mystack.h
	$(CC) -c fid.c
clean:
	rm -f $(OBJ) monte
