/*
 * File: stack.h
 * -------------
 * See file stack.c for details.
 */

#ifndef _stack_h_
#define _stack_h_

/* We use incomplete type here. */
typedef struct StackCDT *Stack;

/* Basic interface functions */
unsigned long GetNumStack(Stack stack);
unsigned long GetLimitStack(Stack stack);
Stack NewStack(unsigned long num,size_t size);
void ClearStack(Stack stack);
void *Push(Stack stack, void *element);
void *Pop(Stack stack);
void *TopElement(Stack stack);
void StackContents(Stack stack, void (*printfun)(void *));
void *GetElementByIndex(Stack stack,unsigned long index);
void CopyStack(Stack s1,Stack s2);
Stack ReadStack(char *fileName,size_t elsize,void *(*readfun)(FILE *,void *));

#endif /* _stack_h_ */
