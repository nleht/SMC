/*
 * File: util.h
 * ------------
 * Contains headers for subroutines useful in various
 * scientific programs.
 * See corresponding file "util.c" for more information
 */

#ifndef _util_h_
#define _util_h_

#include <stdio.h>

/* Boolean type */
#ifdef bool
#undef bool
#endif

#ifdef FALSE
#undef FALSE
#endif
#ifdef TRUE
#undef TRUE
#endif
/* typedef enum {FALSE, TRUE} bool; */
typedef int bool;
#define FALSE 0
#define TRUE 1

#ifdef PI
#undef PI
#endif
#define PI 3.14159265358979323846 /* same as M_PI in math.h */

/* Input/output functions */
FILE *Open(const char *fname,const char *flags);
double **ReadMatrix(char *fileName,int *m,int *n);
double *ReadArray(char *fileName,int *n);
void SaveColumn(char *fileName,double *array,int n);
void SaveMatrix(char *fileName,double **matrix,int m,int n);
void Error(char *format, ...);
/* Interpolation functions */
void TableGen(double *x,double *y,int n,double *xi,double *yi,int ni);
double Interpolate2(double **p,int m,int n,double dx,double dy,
                   double x,double y);
double Interpolate1(double *p,int n, double dy, double y);
double Round(double **p,int m,int n,double dx,double dy,
                   double x,double y);
/* Allocation functions */
double *Allocate1(int n);
int *Allocate1Int(int n);
void *Allocate1all(int n,size_t size);
double **Allocate2(int n1,int n2);
int **Allocate2Int(int n1,int n2);
void **Allocate2all(int n1,int n2,size_t size);
double ***Allocate3(int n1,int n2,int n3);
void Free1(double *arr,int n);
void Free2(double **arr,int n1,int n2);
void Free3(double ***arr,int n1,int n2,int n3);
/* Vector analysis functions */
void CylindricalGradient(double **s,double **dsdr,double **dsdz,
	      double dr,double dz,int m,int n,int irbound,int izbound);
void CartesianGradient(double **s,double **dsdx,double **dsdz,
	      double dx,double dz,int m,int n,int ixbound,int izbound);
void CylindricalDiv(double **pr,double **pz,double **divp,
	 double dr,double dz,int m,int n);
void CartesianDiv(double **px,double **pz,double **divp,
	 double dx,double dz,int m,int n);
void SmallRotate(double *ax,double *ay,double *az,double dpx,double dpy,double dpz);
void Rotate(double *ax,double *ay,double *az,double dpx,double dpy,double dpz);
void Cross(double ax,double ay,double az,
		double bx,double by,double bz,double *cx,double *cy,double *cz);
void FindOrthogonalVector(double px,double py,double pz,double dpx,double dpy,
		double *rx,double *ry,double *rz);
void ConvertToLabFrame1(double px,double py,double pz,
		double *x,double *y,double *z);
void ConvertToLabFrame(double px,double py,double pz,
		double *x,double *y,double *z);
void ConvertToParticleFrame1(double px,double py,double pz,
		double *x,double *y,double *z);
void ConvertToParticleFrame(double px,double py,double pz,
		double *x,double *y,double *z);
/* Various mathematical functions */
double ExpIntegralE1(double x);
double ExpIntegralE1Inv(double y);
void InitMyRandom(int rseed);
double MyRandom(void);

#endif /* _util_h_ */
