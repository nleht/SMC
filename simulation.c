/*
 * File: simulation.c
 * ------------------
 * 2018-02-27: New class "SimIOManager" for input-output stuff; capability of probability simulations
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "interface.h"
#include "util.h"
#include "world.h"
#include "simulation.h"
#include "particle.h"
#include "mystack.h"
#include "fid.h"
#include "wait.h"
#include "chain.h"

/********* TYPE ***********/

struct SimIOManagerCDT {
	bool uniform_world; // different IO for uniform/nonuniform worlds
	/* Multiple outputs */
	int numOutputs,nextOutput;
	double *outputTimes;
	Chain psfname; /* phase space file name */
	Chain edfname; /* deposited energy file name */

	/* Dimensionless parameters for uniform simulation only */
	int ndelta0,neta0,nmu0;
	int ideltaini,ietaini,imuini;
	double *Delta0,*Eta0,*Mu0;

	Chain bsfname; /* backscattered electrons file name */
	Chain gfname;	//ground electron file name
	
	Chain sumpsfname;
	Chain sumbsfname;
	Chain sumtimefname;
	Chain sumgfname;
	bool needLog;
	char logFname[256];

	/* The summary printouts file names */
	bool needSummary;

	/* Addition: probability */
	Probability probability;
	Chain probfname;
};

struct ProbabilityCDT {
	unsigned long numStop;
	double deltat;
	unsigned long numTries;
	bool stopOnTotals;
	unsigned long numGoal;
	double eGoal;
}; 

struct SimulationCDT {
	World world;
	unsigned long numtot;
		 /* Total number of electrons, (including thermalized) AND photons */
	unsigned long maxnum;
		/* maximum particle number allowed at the same time moment */
	Particle tmpParticle; /* temporary particle storage */

	Stack distribution; /* array to store the info before output */

	unsigned long minGoal; /* if the number of particles reduces, we "split" them to get this number */
	bool useUpperBound; /* Reduce the number of particles stochastically */
	unsigned long upperBound,maxGoal;

	Stack backscatter; /* backscattered electrons - only for nonuniform */

	Stack ground; //electrons that reach the ground

	/* Variables for stacked-particle simulations only */
	Stack stack;
	unsigned long maxStackDepth;

	/* The simulations with different charges */
	int nCharge;
	double *charges;
	double time;
};

/********** STATIC FUNCTIONS ************/
static void InitStackedParticle(Simulation s);
static void InitSaveSummary(SimIOManager iom);
static int GetFileSize(Chain fname);
static void CleanFile(Chain fname);
static void CleanSummaryFiles(SimIOManager iom);
static void InitCharges(Simulation s);
static void InitDimensionlessParameters(SimIOManager iom);
static void RunMultipleUniformSimulation(Simulation s, SimIOManager iom, FIDT fid);
static void RunMultipleNonUniformSimulation(Simulation s, SimIOManager iom, FIDT fid);
static void RunSimulationForAllInitialDistributions(Simulation s, SimIOManager iom, FIDT fid);
static void RunSingleSimulation(Simulation s, SimIOManager iom);
static void FillInitialDistribution(Simulation s, FIDT fid, int iRun, bool verbose);
static void FillDistribution(Simulation s,double resizeCoef, bool verbose);
/* Stacked-particle */
static double RunProbabilitySimulation(Simulation s, SimIOManager iom, FIDT fid, int i);
static bool RunSuccessSimulation(Simulation s, Probability prob);
//static bool RunSuccessSimulation(Simulation s, unsigned long numStop, double deltat, unsigned long numGoal, double eGoal);
static void RunStackedParticleSimulation(Simulation s, unsigned long numStop, double tfin, bool verbose);
static double GetMomentStack(Stack s, int code, void *param);
//static double GetEnergyStack(Stack s);
/* Auxiliary */
static void Step(Simulation s,Particle particle, double tfin);
static void DropParticle(Simulation s);
static void PutParticle(Simulation s,Particle e);
static void DumpDistribution(Simulation s, SimIOManager iom, int number);
static void DumpBackscatter(Simulation s, SimIOManager iom, int number);
static void DumpGround(Simulation s, SimIOManager iom, int number);
static void DumpStack(Stack s,Chain fname,int number); /* auxiliary */
static void DumpDepositEnergy(Simulation s, SimIOManager iom, int number);
static void PrintSummary(Simulation s);
//static void GetSummary(Simulation s, double *psnum, double *bsnum, double *gnum);
static void WriteSummaryValue(double v, Chain f);
static void SaveSummary(Simulation s, SimIOManager iom);
static void AppendFileNames(SimIOManager iom,char *suf);
static void RestoreFileNames(SimIOManager iom);
static void AppendRestoreFileNames(SimIOManager iom,char *suf,bool append);


Simulation InitSimulation(void)
{
	Simulation s;
	long maxnum,minGoal,maxGoal,upperBound;

	if(!(s=malloc(sizeof(struct SimulationCDT))))
		Error("InitSimulation: Not enough memory");
	if(!(s->tmpParticle=malloc(ParticleMemUsage())))
		Error("InitSimulation: Not enough memory");

	GetNumbers("Maximum number of particles?","l",&maxnum);
	s->maxnum=maxnum;
	s->distribution=NewStack(s->maxnum,ParticleMemUsage());
	/* Initialize the parts classes */
	s->world=InitWorld();
	if(!IsUniform(s->world) && UseE(s->world)) InitCharges(s);
	InitParticleClass(s->world);
	GetNumbers("Min. to be watched?","l",&minGoal);
	s->minGoal=minGoal;
	GetNumbers("Infinite growth (y/n)?","b",&(s->useUpperBound));
	if(s->useUpperBound){
		GetNumbers("Max. to be watched and when to resize?","ll",&maxGoal,&upperBound);
		if(maxGoal>=upperBound) Error("The second number should be larger");
		s->maxGoal=maxGoal; s->upperBound=upperBound;
	}
	InitStackedParticle(s);
	return s;
}

// Public member (at least, for reading)
World GetWorld(Simulation s){ return s->world; }

static void InitStackedParticle(Simulation s)
{
	/* GetNumbers("Maximum stack depth?","l",&maxStackDepth); */
	s->maxStackDepth=s->maxnum;
	s->stack=NewStack(s->maxStackDepth,ParticleMemUsage());
	if(!IsUniform(s->world)){
		InitDepositEnergy(s->world); /* in the "world" class */
		s->backscatter=NewStack(s->maxnum,ParticleMemUsage());
		s->ground=NewStack(s->maxnum,ParticleMemUsage());
	}else{
		s->backscatter=NULL;
		s->ground=NULL;
	}
}

Probability InitProbability(void)
{
	Probability prob;
	if(!(prob=malloc(sizeof(struct ProbabilityCDT))))
		Error("InitProbability: Not enough memory");
	GetNumbers("Maximum number to be tracked:","l",&(prob->numStop));
	GetNumbers("Time step:","f",&(prob->deltat));
	GetNumbers("Number of tries:","l",&(prob->numTries));
	GetNumbers("Stop on total number and total energy (y) or number of electrons exceeding given energy (n)?","b",&(prob->stopOnTotals));
	GetNumbers("Goal number:","l",&(prob->numGoal));
	GetNumbers("Goal energy:","f",&(prob->eGoal));
	return prob;
}


SimIOManager InitIOManager(bool uniform_world)
{
	SimIOManager iom;
	bool is_prob;
	int i;
	static char buf[80]; /* static to allocate in the heap */

	if(!(iom=malloc(sizeof(struct SimIOManagerCDT))))
		Error("InitIOManager: Not enough memory");

	iom->uniform_world = uniform_world;
	GetNumbers("Is this a probability simulation (y/n)?","b",&is_prob);
	if (is_prob) {
		GetNumbers("<PROBABILITY>","");
		iom->probability = InitProbability();
		GetNumbers("Probability output file name (without \".dat\"):","s",80,buf);
		iom->probfname=NewChain(buf);
		iom->psfname=NewChain("notused");
		iom->edfname=NewChain("notused");
		iom->bsfname=NewChain("notused");
		iom->gfname=NewChain("notused");
		GetNumbers("</PROBABILITY>","");
	} else {
		GetNumbers("<INPUT-OUTPUT>","");
		iom->probability = NULL;
		iom->probfname = NewChain("notused");
		GetNumbers("Distribution output file name (without \".dat\"):","s",80,buf);
		iom->psfname=NewChain(buf);
		if(!uniform_world){
			GetNumbers("Deposited energy output file name (without \".dat\"):","s",80,buf);
			iom->edfname=NewChain(buf);
			GetNumbers("Backscattered electron distribution output file name (without \".dat\"):",
				"s",80,buf);
			iom->bsfname=NewChain(buf);
			GetNumbers("Ground electron distribution output file name (without \".dat\"):",
				"s",80,buf);
			iom->gfname=NewChain(buf);
		}else{
			iom->bsfname=NewChain("notused");
			iom->gfname=NewChain("notused");
			iom->edfname=NewChain("notused");
			/*strcpy(s->bsfname,"notused");
			strcpy(,"notused");*/
		}
		GetNumbers("How many outputs?","d",&(iom->numOutputs));
		if(iom->numOutputs <= 0) Error("InitMultipleOutputs: incorrect number of outputs");
		iom->outputTimes=Allocate1(iom->numOutputs);
		GetNumbers("Enter the times:","");
		for(i = 0; i < iom->numOutputs; i++)
			GetNumbers("","f",&(iom->outputTimes[i]));
		GetNumbers("Need summary (y/n)?","b",&iom->needSummary);
		if(iom->needSummary) InitSaveSummary(iom);
		GetNumbers("</INPUT-OUTPUT>","");
	}
	return iom;
}

static void InitSaveSummary(SimIOManager iom)
{
	static char buf[256]; /* static to allocate in the heap */
	GetNumbers("Number summary file name (no \".dat\")?","s",80,buf);
	iom->sumpsfname=NewChain(buf);
	if(!(iom->uniform_world)){
		GetNumbers("Backscattered summary file name (no \".dat\")?","s",80,buf);
		iom->sumbsfname=NewChain(buf);
				//Addition
		GetNumbers("Ground summary file name (no \".dat\")?","s",80,buf);
		iom->sumgfname=NewChain(buf);
				//End Addition
	}
	GetNumbers("Time summary file name (no \".dat\")?","s",80,buf);
	iom->sumtimefname=NewChain(buf);
}

static void CleanFile(Chain fname) {
	static char buf[256]; /* static to allocate in the heap */
	FILE *file;	
	sprintf(buf,"%s.dat",ChainString(fname));
	file=Open(buf,"w"); fclose(file);
}

static int GetFileSize(Chain fname) {
	static char buf[256]; /* static to allocate in the heap */
	double **tmp;
	int n1,n2,res;
	FILE *file;	
	sprintf(buf,"%s.dat",ChainString(fname));
	if(!(file=fopen(buf,"r"))){
		res = 0;
	} else {
		fclose(file);
		tmp=ReadMatrix(buf,&n1,&n2);
		Free2(tmp,n1,n2);
		res = n1*n2;
	}
	return res;
}

static void CleanSummaryFiles(SimIOManager iom) {
	/* Clear the files */
	CleanFile(iom->sumpsfname);
	if(!(iom->uniform_world)){
		CleanFile(iom->sumbsfname);
		CleanFile(iom->sumgfname);
	}
	CleanFile(iom->sumtimefname);
}

static void InitCharges(Simulation s)
{
	int i;
	GetNumbers("How many simulation with different charges?","d",&(s->nCharge));
	s->charges=Allocate1(s->nCharge);
	GetNumbers("Enter the charges (C):","");
	for(i=0;i<s->nCharge;i++){
		GetNumbers("","f",&(s->charges[i]));
	}
}

static void InitDimensionlessParameters(SimIOManager iom)
{
	int idelta0,ieta0,imu0;

	/* Input the table of dimensionless parameters delta0,eta0,mu0 */
	GetNumbers("Need multiple uniform log file (y/n)?","b",&(iom->needLog));
	GetNumbers("Multiple uniform log file name (used if answered y):","s",80,iom->logFname);
	if (!(iom->needLog)) Printf("Skipping log creation\n");
	GetNumbers("Number of deltas:","d",&(iom->ndelta0));
	if(iom->ndelta0 > 0){
		iom->Delta0=Allocate1(iom->ndelta0);
		GetNumbers("delta0 values:","");
		for (idelta0 = 0; idelta0 < iom->ndelta0; idelta0++)
			GetNumbers("","f",&(iom->Delta0[idelta0]));
	}
	GetNumbers("Number of etas:","d",&(iom->neta0));
	if(iom->neta0 > 0){
		iom->Eta0=Allocate1(iom->neta0);
		GetNumbers("eta0 values:","");
		for (ieta0 = 0; ieta0 < iom->neta0; ieta0++)
			GetNumbers("","f",&(iom->Eta0[ieta0]));
	}
	GetNumbers("Number of mus:","d",&(iom->nmu0));
	if(iom->nmu0>0){
		iom->Mu0=Allocate1(iom->nmu0);
		GetNumbers("mu0 values:","");
		for (imu0 = 0; imu0 < iom->nmu0; imu0++)
			GetNumbers("","f",&(iom->Mu0[imu0]));
	}
	GetNumbers("Initial numbers to tag delta0, eta0, and mu0:",
		"ddd",&(iom->ideltaini),&(iom->ietaini),&(iom->imuini));
}

void RunSimulation(Simulation s, SimIOManager iom, FIDT fid)
{
	if(IsUniform(s->world)){
		InitDimensionlessParameters(iom);
	}
	StartCalculations();
	if(IsUniform(s->world)){
		RunMultipleUniformSimulation(s,iom,fid);
	}else{
		Printf("Non-uniform\n");
		RunMultipleNonUniformSimulation(s,iom,fid);
	}
}

static void RunMultipleNonUniformSimulation(Simulation s, SimIOManager iom, FIDT fid)
{
	int i;
	static char suf[80]; /* static to allocate in the heap */
	if(UseE(s->world)){
		for(i = 0; i < s->nCharge; i++){
			sprintf(suf,"_c%d",i);
			AppendFileNames(iom,suf);
			Printf("\n%d-th charge\n",i);
			UpdateNonUniformWorld(s->world,s->charges[i]);
			RunSimulationForAllInitialDistributions(s,iom,fid);
			RestoreFileNames(iom);
		}
	}else{
		RunSimulationForAllInitialDistributions(s,iom,fid);
	}
}

static void RunMultipleUniformSimulation(Simulation s, SimIOManager iom, FIDT fid)
{
	FILE *logFile;
	int idelta0,ieta0,imu0;
	static char suf[256]; /* static to allocate in the heap */

	Printf("Begin RunMultipleUniformSimulation\n");
	/*strcpy(psbuf,s->psfname);*/ /* save the generic phase space name */
	if(!(iom->uniform_world)) Error("RunMultipleUniformSimulation: not for non-uniform");
	/* For each combination of delta0,eta0 and mu0, run the Monte Carlo simulation */
	if (iom->needLog) {
		logFile=Open(iom->logFname,"w"); fclose(logFile); // Clean the file
	}
	for(idelta0 = 0; idelta0 < iom->ndelta0; idelta0++){
		for(ieta0 = 0; ieta0 < iom->neta0; ieta0++){
			for(imu0 = 0; imu0 < iom->nmu0; imu0++){
				UpdateUniformWorld(s->world,iom->Delta0[idelta0],iom->Eta0[ieta0],iom->Mu0[imu0]);
				Printf("\nStarting a new simulation with parameters delta0=%g, eta0=%g, mu0=%g\n",
					iom->Delta0[idelta0],iom->Eta0[ieta0],iom->Mu0[imu0]);
				/* Get the correct file name */
				sprintf(suf,"_d%d_e%d_m%d",
					idelta0 + iom->ideltaini,ieta0 + iom->ietaini,imu0 + iom->imuini);
				AppendFileNames(iom,suf);
				/*sprintf(iom->psfname,"%s_%d_%d_%d",psbuf,
					idelta0+iom->ideltaini,ieta0+iom->ietaini,imu0+iom->imuini);*/
				/* the files bsfname and edfname are not used */
				/***************************/
				RunSimulationForAllInitialDistributions(s, iom, fid);
				/***************************/
				RestoreFileNames(iom);
				/* Make a record about the run */
				if (iom->needLog) {
					logFile=Open(iom->logFname,"a");
					fprintf(logFile,"%10d\t%10d\t%10d\t%12.3e\t%12.3e\t%12.3e\n",
						idelta0 + iom->ideltaini, ieta0 + iom->ietaini, imu0 + iom->imuini,
						iom->Delta0[idelta0],iom->Eta0[ieta0],iom->Mu0[imu0]);
					fclose(logFile);
				}
			}
		}
	}
	/*strcpy(s->psfname,psbuf);*/ /* restore the generic phase space name */
	Printf("End RunMultipleUniformSimulation\n");
}

static void RunSimulationForAllInitialDistributions(Simulation s, SimIOManager iom, FIDT fid)
{
	int i, istart; /* the number of the initial distribution */
	static char suf[128]; /* static to get it in the heap */
	double p=0;

	if (iom->probability) {
		// Instead of cleaning, extend
		istart = GetFileSize(iom->probfname);
		Printf("New/resume probability simulation using %s.dat, starting with %d\n",ChainString(iom->probfname),istart);
	} else istart = 0;
	for(i = istart; i < GetNumberOfInitialDistributions(fid); i++){
		/* choose the correct file names */
		Printf("\n%d-th initial distribution\n",i);
		if (iom->probability) {
			// WARNING!!! THE FOLLOWING OPTIMIZATION IS VERY DANGEROUS, REMOVE WHEN DONE!!!
			// If reached p==1.0, don't calculate it anymore (save time by assuming stuff)
			//if (p<1.0) 
			p = RunProbabilitySimulation(s,iom,fid,i);
			WriteSummaryValue(p,iom->probfname);
		} else {
			sprintf(suf,"_i%d",i);
			AppendFileNames(iom,suf);
			FillInitialDistribution(s,fid,i,TRUE); /* read initial information elsewhere */
			RunSingleSimulation(s,iom);
			RestoreFileNames(iom);
		}
	}
}

static double RunProbabilitySimulation(Simulation s, SimIOManager iom, FIDT fid, int i)
{
	Probability prob;
	double p;
	unsigned long iTry, success;

	prob = iom->probability;
	Printf("\nBegin RunProbabilitySimulation(stopOnTotals=%s,numTries=%ld,numGoal=%ld,eGoal=%lf)\n",
		(prob->stopOnTotals ? "y" : "n"),prob->numTries,prob->numGoal,prob->eGoal);
	success = 0;
	for (iTry = 0; iTry < prob->numTries; iTry++){
		FillInitialDistribution(s,fid,i,FALSE); /* read initial information elsewhere */
		if (RunSuccessSimulation(s, prob)) success++;
	}
	p = (double)success/(double)(prob->numTries);
	Printf("p = %g; %ld successes after %ld tries\n",p,(long)success,(long)prob->numTries);			
	Printf("End RunProbabilitySimulation\n\n");
	return p;
}

/*
 * RunSuccessSimulation
 * --------------------
 * Copied from RunSingleSimulation. Runs until certain criteria are met, returns if it succeeded.
 */
static bool RunSuccessSimulation(Simulation s, Probability prob)
{
	unsigned long psnum, numIni;
	double pse, tfin, ps_numgt1mev;
	bool stopCondition;

	/* Sanity check */
	numIni = GetNumStack(s->distribution);
	if (!GetParticleClassProperty("CREATE") && (prob->numGoal > numIni))
		Error("If no particles created, the number %ld will never be reached from %ld",prob->numGoal,numIni);
	if(!IsUniform(s->world)){
		ClearDepositEnergy(s->world);
		ClearStack(s->backscatter);
		ClearStack(s->ground);
	}
	tfin = 0;
	while (TRUE) {
		tfin += prob->deltat;
		//Printf("tfin = %f\n",tfin);
		FillDistribution(s,1,FALSE); /* copy from s->distribution to whereever needed */
		RunStackedParticleSimulation(s,prob->numStop,tfin,FALSE);
		if (GetNumStack(s->stack)!=0) Error("Stack=%ld\n",GetNumStack(s->stack));
		/* Save the results */
		psnum = GetNumStack(s->distribution); // total # of particles but without weights
		if (psnum >= prob->numStop) return TRUE;
		if (prob->stopOnTotals) {
			pse = GetMomentStack(s->distribution,1,NULL); // total energy of particles
			stopCondition = ((psnum >= prob->numGoal) && (pse >= prob->eGoal));
		} else {
			ps_numgt1mev = GetMomentStack(s->distribution,2,&(prob->eGoal)); // # > 1MeV particles, including weights
			stopCondition = (ps_numgt1mev >= prob->numGoal);
		}
		if (stopCondition) return TRUE;
		else if (psnum == 0) return FALSE;
		// otherwise, continue simulation
		//Printf("psnum == %ld\n",psnum);
	}
}

static void RunSingleSimulation(Simulation s, SimIOManager iom)
{
	int iOutput;
	double resizeCoef;
	bool verbose;

	Printf("\nBegin RunSingleSimulation\n");
	if(iom->needSummary) CleanSummaryFiles(iom);
	/*Printf("The file names:\ns->psfname=\"%s\"\ns->bsfname=\"%s\"\ns->edfname=\"%s\"\n",
		ChainString(s->psfname),ChainString(s->bsfname),ChainString(s->edfname)); getchar(); */
	/*FIDContents(s->fid);*/
	for(iOutput = 0; iOutput < iom->numOutputs; iOutput++){
		/*split=1;*/
		if(GetNumStack(s->distribution)==0){
			Printf("No avalanche\n");
			break;
		}
		resizeCoef=((double)s->minGoal)/((double)GetNumStack(s->distribution));
		if(resizeCoef < 1){
			/* do not split particles, but maybe reduce their number */
			if(s->useUpperBound && GetNumStack(s->distribution) > s->upperBound)
				resizeCoef=((double)s->maxGoal)/((double)GetNumStack(s->distribution));
			else
				resizeCoef=1;
		}
		Printf("Simulation #%d\n",iOutput);
		FillDistribution(s,resizeCoef,TRUE); /* copy from s->distribution to s->stack */
		if(!IsUniform(s->world)){
			ClearDepositEnergy(s->world);
			ClearStack(s->backscatter);
			ClearStack(s->ground);
		}
		ResetWait();
		RunStackedParticleSimulation(s,0UL,iom->outputTimes[iOutput],verbose=TRUE);
		/* Save the results */
		if(iom->needSummary) SaveSummary(s,iom);
		DumpDistribution(s,iom,iOutput);
		if(!IsUniform(s->world)){
			DumpBackscatter(s,iom,iOutput);
			DumpGround(s,iom,iOutput);
			DumpDepositEnergy(s,iom,iOutput);
		}
	}
	Printf("End RunSingleSimulation\n\n");
#if PENETRD
	Printf("zmin=%g\n\n",GetZmin());
#endif
}

static void FillInitialDistribution(Simulation s, FIDT fid, int iRun, bool verbose)
{
	Particle e;

	if (verbose) { Printf("Begin FillInitialDistribution(s,iRun=%d)\n",iRun); fflush(stdout); }
	e=s->tmpParticle; /* pre-allocated particle memory space */
	s->numtot=0; /* no particles */
	/* Initial electron information */
	ResetFID(fid,iRun,verbose);
	ClearStack(s->distribution);
	while(CopyNextFromFID(fid,e)){
		if(isnan(GetParticleEnergy(e))){ PrintParticleInfo(e); exit(1); }
		SetParticleID(e,s->numtot++);
		/*SetParticleWeight(e,1);*/
		/* for debugging */
		/* PrintParticleInfo(e); */
		if(!(Push(s->distribution,e)))
			Error("FillInitialDistribution: Stack overflow");
	}
	// StackContents(s->distribution,PrintParticleInfo);
	if (verbose) { Printf("End FillInitialDistribution\n"); fflush(stdout); }
}

/*
 * FillDistribution
 * ----------------
 * Take the "distribution" stack and copy the information for the next simulation,
 * adding a split factor
 */
static void FillDistribution(Simulation s,double resizeCoef, bool verbose)
{
	Particle e;
	int i,split;

	if (verbose) Printf("Begin FillDistribution(s,%g)\n",resizeCoef);
	if(resizeCoef<=0) Error("FillDistribution: bad resize %g",resizeCoef);
	split=(int)floor(resizeCoef); /* >= 0 */
	/* Explain what we do */
	if (verbose) {
		if(split > 1){
			Printf("FillDistribution: splitting %d times\n",split);
		}else if(split < 1){
			Printf("FillDistribution: reducing the number by a factor %g\n",resizeCoef);
		}else{
			Printf("FillDistribution: number of particles unchanged\n");
		}
	}
	if (GetNumStack(s->stack)!=0) {
		Printf("WARNING: Stack=%ld\n",GetNumStack(s->stack));
		ClearStack(s->stack);
	}
	while((e=Pop(s->distribution))!=NULL){
		/* SetParticleWeight(e,GetParticleWeight(e)/split); */
		if(split>1){
			MultiplyWeight(e,1./((double)split));
			for(i=0;i<split;i++) PutParticle(s,e);
		}else if(split<1){
			/* Get rid of some particles randomly */
			if(MyRandom() < resizeCoef){
				MultiplyWeight(e,1./resizeCoef);
				PutParticle(s,e);
			}
		}else{ /* split=1, no change in the particle number */
			PutParticle(s,e);
		}
	}
	if (verbose) Printf("End FillDistribution\n");
}


// Managed to clear all use of SimIOManager from this function!
static void RunStackedParticleSimulation(Simulation s, unsigned long numStop, double tfin, bool verbose)
{
	unsigned long maxtop, num_on_return;
	Particle particle,ep;

	if (verbose){
		Printf("Begin RunStackedParticleSimulation\n");
		Printf("Initial energy=%g\n",GetMomentStack(s->stack,1,NULL));
	}
	/* The main cycle */
	maxtop=0;
	//ResetWait(); // moved to the wrapping function
	while(TRUE){
		/* Print something periodically */
		if(Wait()) PrintSummary(s);
		if ((numStop > 0) && (GetNumStack(s->stack)+GetNumStack(s->distribution) >= numStop)){
			num_on_return = GetNumStack(s->stack)+GetNumStack(s->distribution);
			if (verbose) Printf("The goal number %ld+%ld>=%ld reached, stopping!\n",
				GetNumStack(s->stack), GetNumStack(s->distribution), numStop);
			// Transfer all remaining particles to the "distribution" stack
			CopyStack(s->stack,s->distribution);
			ClearStack(s->stack);
			// Sanity check
			if (GetNumStack(s->distribution) != num_on_return)
				Error("%ld != %ld\n",GetNumStack(s->distribution),num_on_return);
			break;
		}
		/* Careful: we don't create a local copy of electron! */
		if(!(particle=TopElement(s->stack))) break;
		Step(s,particle,tfin);
		if(GetNumStack(s->stack) > maxtop){
			maxtop=GetNumStack(s->stack);
			ep=TopElement(s->stack);
			if (verbose) {
				Printf("New particle number maximum reached: %ld elements, t=%g\n",maxtop,GetParticleTime(ep));
				PrintSummary(s);
			}
		}
	}
	s->time = tfin;
	if (verbose) {
		PrintSummary(s);
		Printf("End RunStackedParticleSimulation\n");
	}
}


/*
 * Step
 * ----
 * The basic subroutine.
 */
static void Step(Simulation s,Particle particle, double tfin)
{
	ParticleStatusT status;
	//double px,py,pz;
	double x,y,z;

	status=GeneralParticleStep(particle,s->tmpParticle, tfin);

	switch(status){
	case NOTHING:
		/*Printf("\n ***NOTHING***\n");*/
		break;
	case DISAPPEAR: /* electron thermalizes, photon gets photoelectrically absorbed */
		/* Nothing to unload */
		/*Printf("t");*/
		if(!IsUniform(s->world)) DepositAllEnergy(particle); /* Deposit the energy */
		DropParticle(s);
		/*Printf("\n ***REMOVE***\n");*/  /*Printf("-");*/
		break;
	case EXPIRE:
		if(!Push(s->distribution,particle)){
			Printf("Stack=%ld\n",GetNumStack(s->distribution));
			StackContents(s->distribution,PrintParticleInfo);
			Error("Results stack overflow");
		}
		DropParticle(s);
		break;
	case LEAVE: /* only for nonuniform */
		/*Printf("b");*/
		//Figure out if the electron is backscattered or reaches ground
				//Addition
		//GetParticleMomentum(particle,&px,&py,&pz);
		GetParticleCoordinates(particle,&x,&y,&z);
		if(z < 0.001)
		{
			if(!Push(s->ground,particle))
			Error("Ground stack overflow");
		}
		else{
			//End Addition
			if(!Push(s->backscatter,particle))
			Error("Backscattered stack overflow");
		}
		DropParticle(s);
		break;
	case DOUBLE: /* electron causes ionization */
		/*Printf("i");*/
		SetParticleID(s->tmpParticle,s->numtot++);
		PutParticle(s,s->tmpParticle);
		/*Printf("\n ***NEW***\n");*/
		/* Printf("+%ld:%lf\n",GetParticleKind(s->tmpParticle),GetParticleEnergy(s->tmpParticle)); */
		break;
	default:
		Error("Unknown electron status");
		break;
	}
		/* for debugging */
		/* StackContents(s->stack,PrintElectronInfo); */
}

/*
 * DropParticle, PutParticle
 * -------------------------
 * Remove or add a particle to a simulation.
 * Good for any type of simulation.
 */
static void DropParticle(Simulation s)
{
	Pop(s->stack);
}

static void PutParticle(Simulation s,Particle e)
{
	if(!Push(s->stack,e))
		Error("Runaway Stack overflow");
}

/* Good for any type of simulation */
static void DumpDistribution(Simulation s, SimIOManager iom, int number)
{
	Printf("Saving distribution #%d, %lu particles to \"%s_%d.dat\" ... ",
		number,GetNumStack(s->distribution),ChainString(iom->psfname),number);
	DumpStack(s->distribution, iom->psfname, number);
	Printf("done\n");
}

/* Good for nonuniform (backscatter) simulation */
static void DumpBackscatter(Simulation s, SimIOManager iom, int number)
{
	Printf("Saving backscatter #%d, %lu particles to \"%s_%d.dat\" ... ",
		number,GetNumStack(s->backscatter),ChainString(iom->bsfname),number);
	DumpStack(s->backscatter,iom->bsfname,number);
	Printf("done\n");
}

// Good for nonuniform (ground) simulation
				//Addition
static void DumpGround(Simulation s, SimIOManager iom, int number)
{
	Printf("Saving ground #%d, %lu particles to \"%s_%d.dat\" ...",
		number, GetNumStack(s->ground),ChainString(iom->gfname),number);
	DumpStack(s->ground,iom->gfname,number);
	Printf("done\n");
}

				//End Addition

static void DumpStack(Stack s, Chain fname, int number)
{
	FILE *file;
	Particle particle;
	static char filename[100]; /* static to allocate in the heap */
	unsigned long i;

	sprintf(filename,"%s_%d.dat",ChainString(fname),number);
	file=Open(filename,"w");
	i=0;
	while((particle=GetElementByIndex(s,i++))!=NULL)
		DumpParticleLine(particle,file);
	fclose(file);
}

static void DumpDepositEnergy(Simulation s, SimIOManager iom, int number)
{
	static char filename[100];
	sprintf(filename,"%s_%d.dat",ChainString(iom->edfname),number);
	OutputDepositEnergy(s->world,filename);
}

/*
 * GetMomentStack
 * --------------
 * code:
 * 0 - number of particles (taking into account weights);
 * 1 - total energy;
 * 2 - number of particles > certain energy (given by param)
 */
static double GetMomentStack(Stack s, int code, void *param)
{
	Particle e;
	int i;
	double res;

	i=0; res=0;
	while((e=GetElementByIndex(s,i++))!=NULL){
		// Printf("%d: weight=%lg,energy=%lg,kind=%d\n",i,GetParticleWeight(e),GetParticleEnergy(e),GetParticleKind(e));
		if(isnan(GetParticleWeight(e)) || isnan(GetParticleEnergy(e))) {
			PrintParticleInfo((Particle)e); exit(1);
		}
		/* if(isnan(GetParticleEnergy(e)))
			Error("energy=nan, %d",i); */
		switch (code) {
		case 0: res += GetParticleWeight(e); break;
		case 1: res += GetParticleWeight(e)*GetParticleEnergy(e); break;
		case 2: if (GetParticleEnergy(e)>*((double*)param)) res += GetParticleWeight(e); break;
		default: Error("Unknown moment: %d",code);
		}
	}
	return res;
}

static void PrintSummary(Simulation s)
{
	double enraw,endep,enbs,ensurv,engnd; /*for debugging */

	Printf("Stack=%lu,Result=%lu,endtime=%g\n",
	       GetNumStack(s->stack),GetNumStack(s->distribution),s->time);
	if(!IsUniform(s->world)){
		Printf("Backscattered=%lu\n",GetNumStack(s->backscatter)); // not the same as GetMomentStack(s->..,0) !!!
		Printf("Ground=%lu\n",GetNumStack(s->ground));
		enraw=GetMomentStack(s->stack,1,NULL);
		endep=GetTotalDepositedEnergy(s->world);
		enbs=GetMomentStack(s->backscatter,1,NULL);
		engnd=GetMomentStack(s->ground,1,NULL);
		ensurv=GetMomentStack(s->distribution,1,NULL);
		Printf("Energy: raw=%g, dep=%g, bs=%g, gnd=%g, surv=%g, total=%g\n",
		       enraw,endep,enbs,engnd,ensurv,enraw+endep+enbs+engnd+ensurv);
	}
	fflush(stdout);
}

static void WriteSummaryValue(double v, Chain f) {
	static char fname[256]; /* static to allocate in the heap */
	FILE *file;
	sprintf(fname,"%s.dat",ChainString(f));
	file=Open(fname,"a");
	fprintf(file,"%12.7e\n",v);
	fclose(file);
}

static void SaveSummary(Simulation s, SimIOManager iom) {
	WriteSummaryValue(GetMomentStack(s->distribution,1,NULL), iom->sumpsfname);
	if(!IsUniform(s->world)){
		/* The particles that left the system ("backscattered") */
		WriteSummaryValue(GetMomentStack(s->backscatter,1,NULL),iom->sumbsfname);
		WriteSummaryValue(GetMomentStack(s->ground,1,NULL),iom->sumgfname);
	}
	/* The time */
	WriteSummaryValue(s->time, iom->sumtimefname);
}

/*
 * AppendFileNames, RestoreFileNames
 * ---------------------------------
 * Addition and removal of various numbers characterizing a particular single simulation
 */
static void AppendFileNames(SimIOManager iom,char *suf)
{
	/*Printf("Appending .. \"%s\"\n",suf);*/
	AppendRestoreFileNames(iom,suf,TRUE);
}

static void RestoreFileNames(SimIOManager iom)
{
	/*Printf("Restoring .. \n");*/
	AppendRestoreFileNames(iom,(char *)NULL,FALSE);
}

static void AppendRestoreFileNames(SimIOManager iom,char *suf,bool append)
{
	if (!(iom->probability)) {
		AppendRestoreChain(iom->psfname,suf,append);
		if(iom->needSummary){
			AppendRestoreChain(iom->sumpsfname,suf,append);
			AppendRestoreChain(iom->sumtimefname,suf,append);
		}
		if(!(iom->uniform_world)){
			AppendRestoreChain(iom->bsfname,suf,append);
			AppendRestoreChain(iom->edfname,suf,append);
			AppendRestoreChain(iom->gfname,suf,append);
			if(iom->needSummary){
				AppendRestoreChain(iom->sumbsfname,suf,append);
				AppendRestoreChain(iom->sumgfname,suf,append);
			}
		}
	} else {
		AppendRestoreChain(iom->probfname,suf,append);
		//Printf("Probability: Appending %s to nothing\n",suf);
	}
}

