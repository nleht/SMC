/*
 * File: crossec.c
 * ---------------
 * The physics
 * "In inverse seconds" here means in units of 1/tau = 2*pi*Nm*Zm*r0^2*c =.
 * Could be inverse seconds for appropriate Nm.
 */

#include <math.h>
#include "util.h"
#include "crossec.h"

/******* STATIC FUNCTIONS ********/
static double SNorm(double Eini, double E);
static double Xaux(double Eini,double Emin,double E);
static double STotNorm(double Eini,double Emin);
static double RandomEnergy(double Eini,double Emin, double x,bool simple);


/**************************************************************
 *****                PHYSICS FUNCTIONS                   *****
 **************************************************************/


/**************************************************************
 ********     IONIZATION CROSS SECTION FUNCTIONS          *****
 **************************************************************/

/*
 * CollisionRate
 * -------------
 * Total ionization cross-section, multiplied by Zm*Nm*v (in inverse seconds)
 * It characterizes the probability of collision in unit time.
 * See Mathematica file "main.nb", section "The probability of collision"
 */
double CollisionRate(double g,double Emin,bool simple)
{
	double s;	
	if(simple)
		/* Use simplified Moller cross-section - described in the report */
		s=g/sqrt(g*g-1)*(1./Emin-2./(g-1));
	else
		/* Use honestly integrated Moller cross-section */
		s=g/sqrt(g*g-1)*STotNorm(g-1,Emin);
	if(s<0) s=0;
	return s;
}

/*
 * SNorm
 * -----
 * Moller cross-section without num. coefficients
 * that are independent of E (energy of the created electron)
 * Eini is the energy of the initial electron, and the dropped
 * num. coefficients depend on it.
 * It is used in calculating the random energy E.
 * SNorm == d Xaux(Eini,Emin,E) / dE
 * See section "Random energy calculation" in Mathematica file "main.nb".
 */
static double SNorm(double Eini, double E)
{
	double g2,de;
	g2=(Eini+1)*(Eini+1);
	de=Eini-E;
	return 1/E/E - (2*Eini+1)/E/g2/de + 1/de/de + 1/g2;
}

/*
 * Xaux
 * ----
 * Ionization cross section without num. coefficients, integrated
 * over energies of created electron from Emin to E.
 * This function is proportional to x(E), used in calculating the random energy E.
 * Our goal is to invert it.
 * In Mathematica file "main.nb" it is denoted "xaux" and introduced in
 * the section "Integrated cross-section"
 */
static double Xaux(double Eini,double Emin,double E)
{
	double g2, x;
	
	g2=(Eini+1)*(Eini+1);
	x=-1/E - 1/(E-Eini) + 1/Emin + (E-Emin)/g2 - 1/(Eini-Emin) - 
		(1+2*Eini) * log( E*(Eini-Emin)/Emin/(Eini-E) ) / Eini / g2;
	return x;
		
}

/*
 * STotNorm
 * --------
 * Total Moller cross-section (without coefficients in the front).
 * It is equal to Xaux(Eini,Emin,Eini/2)
 */
static double STotNorm(double Eini,double Emin)
{
	double g2;
	g2=(Eini+1)*(Eini+1);
	return (1/Emin-1/(Eini-Emin) + (Eini-2*Emin)/2/g2 - (2*Eini+1)/g2/Eini*log(Eini/Emin-1) );
}



/**************************************************************
 ********              DYNAMIC FRICTION                   *****
 **************************************************************/

/*
 * FdTot
 * -----
 * Dinamic friction force in inverse seconds (real force / m*c),
 * as given by Bethe and Ashkin.
 */
double FdTot(double p)
{
	double g,g2,E,f,Iion;
	Iion=80.5/511000;
	g2=p*p+1;
	g=sqrt(g2);
	E=g-1;
	f = log(sqrt((g2-1)*(g-1))/sqrt(2)/Iion)
		- (2/g-1/g2)*log(2)/2
		+ 1/g2/2 + E*E/g2/16;
	f=2*g2/(g2-1)*f;
	if(f<0) Error("f<0 at energy=%g keV",E*511);
	return f;
}

/*
 * FdIon
 * -----
 * Dinamic friction force in inverse seconds (real force / m*c)
 * due to ionization process only with creation of electrons with energy
 * greater than Emin.
 */
double FdIon(double p, double Emin)
{
	double E,g2,fInorm,f;
	g2=p*p+1;
	E=sqrt(g2)-1;
	/* Correction for the ionization */
	/* See Mathematica file "main.nb" for the definition of "fInorm" */
	if(E>2*Emin){
		fInorm = 2 - E/(E-Emin) + (E*E-4*Emin*Emin)/8./g2
			- (2+4*E+E*E)/g2*log(2*(E-Emin)/E) + log(E/Emin);
		f=g2/(g2-1)*fInorm;
	}else f=0;
	if(f<0) Error("f<0 at energy=%g keV",E*511);
	return f;
}

/**************************************************************
 ********                  DIFFUSION                      *****
 **************************************************************/

/*
 * Theta2Growth
 * ------------
 * Angular diffusion is described by Theta2Growth = d(theta^2)/dt.
 * See Longmire and Longley, also page 125 of notebook #3.
 * Here we substituted Zm=14.5.
 */
double Theta2Growth(double p)
{
	return 29.*sqrt(p*p+1)/p/p/p*log(p*67.5397);
}

/*
 * DiffusionMu
 * -----------
 * Solution of angular diffusion equation, and approximate determination of the cosine
 * of the random angle from it. The angular diffusion coefficient = 1.
 * See mathematica file "angular.nb" and notebook #3, p. 144-147 for explanation.
 */
double DiffusionMu(double t)
{
	double y,mu,tmp;
	
	y=MyRandom();
	if(t<.5){
		tmp=exp(-1./t);
		if(tmp+y*(1-tmp)<1e-100) mu=-0.999999999;
		else mu=2*t*log(tmp+y*(1-tmp))+1;
		if (mu<-0.999999999) mu=-0.999999999;
	}else if(t>10){
		/* use another formula */
		/*printf("\nDiffusionMu: Warning: tau > 10\n");*/
		mu=2*y-1+6*y*(1-y)*exp(-2*t);
	}else{
		/* printf("t=%g, y=%g",t,y); getchar(); */
		tmp=exp(2*t);
		mu=(1./3.)*(-tmp+sqrt(9.-6.*tmp + tmp*tmp + 12.*tmp*y));
	}
	/* if(mu<.5) {printf("t=%g, y=%g, mu=%g",t,y,mu); getchar();} */
	if(mu>1 || mu<-1) Error("Invalid elastic scattering: t=%g, y=%g, mu=%g",t,y,mu);
	return(mu);
}

/**************************************************************
 ********                  IONIZATION                     *****
 **************************************************************/

/*
 * MollerIonization
 * ----------------
 * Calculate the change in momenta after scattering
 */
void MollerIonization(double *px1,double *py1,double *pz1,
	double *px2,double *py2,double *pz2,
	double Emin,ElectronScatteringT type,bool simple)
{
	double g,e,p,px,py,pz,g1,g2,p1,p2,
		ca1,ca2,sa1,sa2,phi,cp,sp,pini2;
	
	/* Save the old momentum */
	px=*px1; py=*py1; pz=*pz1;
	
	pini2=px*px+py*py+pz*pz;
	p=sqrt(pini2); g=sqrt(pini2+1); e=g-1;
	
	g2=RandomEnergy(e,Emin,MyRandom(),simple)+1;
	g1=1+g-g2;
	p1=sqrt(g1*g1-1);
	p2=sqrt(g2*g2-1);
	/* Calculate the momenta of new and old electrons */			
	phi=2*PI*MyRandom();
	cp=cos(phi);
	sp=sin(phi);
	/* Momentum components of created electrons in the tilted
	   coordinate system of the incident electron */
	switch(type){
	case REGULAR:
		/* "Honest" Moller scattering */
		ca1=sqrt((g1-1)*(g+1)/(g1+1)/e);
		ca2=sqrt((g2-1)*(g+1)/(g2+1)/e);
		sa1=sqrt(2*(g2-1)/(g1+1)/e);
		sa2=sqrt(2*(g1-1)/(g2+1)/e);
		
		*px2= p2*sa2*cp;  *py2= p2*sa2*sp;  *pz2=p2*ca2;
		*px1=-p1*sa1*cp;  *py1=-p1*sa1*sp;  *pz1=p1*ca1;
		break;
	case RIGHT_ANGLE:
		/* Right angle scattering */
		*px2=p2*cp;  *py2=p2*sp;  *pz2=0;
		*px1=0;      *py1=0;      *pz1=p1;
		break;
	case FORWARD:
		/* Forward scattering */
		*px1=0; *py1=0; *pz1=p1;
		*px2=0; *py2=0; *pz2=p2;
		break;
	default:
		Error("Unknown scattering type %d",type);
		break;
	}	
	/* Conversion to a laboratory RF */
	ConvertToLabFrame(px,py,pz,px1,py1,pz1);
	ConvertToLabFrame(px,py,pz,px2,py2,pz2);
}

/*
 * RandomEnergy
 * ------------
 * Calculate energy of a secondary given a uniformly distributed
 * random variable x(E)=RandomEnergy(Eini,Emin,E)=Xaux(Eini,Emin,E)/STotNorm(Eini,Emin)
 * from interval in x from [0,1]. The resulting E must be in interval [Emin,Eini/2].
 * We use dXaux/dE==SNorm(Eini,E)
 */
static double RandomEnergy(double Eini,double Emin, double x,bool simple)
{
	double stot,Eapprox,errorlimit,Eapprox1;
	int i;
	if(simple)
		/* use a simplified expression for a cross-section, see "main.nb" */
		return 1. / ((1-x)/Emin + 2*x/Eini);
	else{ /* use Newton's method */
		stot=STotNorm(Eini,Emin);
		/* De-normalize x */
		x*=stot;
		/* Find E by Newton's method */
		Eapprox=(Eini/2+Emin)/2; /* Initial guess */
		errorlimit=1e-8; /* ~ 5e-3 eV */
		/*printf("Emin=%lf, Eini=%lf, Eapprox=%lf\n",Emin, Eini, Eapprox);*/
		i=0;
		while(fabs(Xaux(Eini,Emin,Eapprox)-x) > errorlimit){
			/* Newton's iteration E_{n+1} = E_n - (x(E_n)-x0)/(dx(E_n)/dE) */
			Eapprox1 = Eapprox - (Xaux(Eini,Emin,Eapprox)-x) / SNorm(Eini,Eapprox);
			if(Eapprox1 <= Emin) Eapprox=(Eapprox+Emin)/2; /* Hit lower limit Emin */
			else if(Eapprox1 >= Eini/2) Eapprox=(Eapprox+Eini/2)/2; /* Hit upper limit Eini/2 */
			else Eapprox=Eapprox1;
			/*printf("Eapprox=%lf, error=%lf\n", Eapprox,SIntNorm(Eini,Eapprox)-x);*/
			i++;
			if(i>100) Error("Cannot calculate random energy");
		}
		/*printf("%d iterations: done",i); getchar();*/
		return Eapprox;
	}
}
/*
double RandomEnergy(double Eini, double x)
{
	double p=-.9;
	return pow(pow(Emin,p)*(1-x)+x*pow((Eini/2),p),1/p);
}*/

/*****************************************************************************/
/********                    PHOTON CROSS-SECTIONS                   *********/
/*****************************************************************************/

/*
 * ComptonProb
 * -----------
 * Compton scattering probability per unit time at NmConv.
 * ComptonProb = c*NmConv*sigma
 * sigma = (2*pi*rclass0^2*Zm)*ComptonProb(p) is
 * the cross-section per molecule.
 * Zm=14.5 is the average number of electrons in a molecule.
 * NmConv=1/(2*pi*rclass0^2*c*Zm)/(1 sec) = 4.6e18 m^{-3} is
 * a convenient unit of molecule densities.
 */
double ComptonProb(double p)
{
	if(p<0) Error("bad input for ComptonProb");
	return ((1+p)/p/p/p*(2*p*(1+p)/(1+2*p)-log(1+2*p)) + log(1+2*p)/2/p
		- (1+3*p)/(1+2*p)/(1+2*p));
}

#if 0
/*
 * PhotoProb
 * ---------
 * Photoellectric effect probability per unit time at NmConv.
 */
double PhotoProb(double p)
{
	if(p<0) Error("bad input for PhotoProb");	
	return (5.9e-5)*pow(p,-3.5);
}
#else
/*
 * PhotoProb
 * ---------
 * Photoellectric effect probability per unit time at NmConv.
 */
double PhotoProb(double p)
{
	double ion; /* ionization energy */
	double x; /* auxiliary variable, denoted by \xi on p. 208 in Heitler */
	double f; /* absorption-edge factor */
	int z; /* =7 for nitrogen, 8 for oxygen */
	double prob,sig,tmp,tmp2,ainv,Z;
	
	if(p<0) Error("bad input for PhotoProb");	
	prob=0;
	ainv=137.036; /* inverse of fine structure constant */
	for(z=7;z<=8;z++){
		Z=(double)z;
		ion=Z*Z/2./ainv/ainv;
		if(p<ion){
			/* L-shell photoeffect - not implemented */
			p=1./511.;
			/*Error("PhotoProb: The energy is too small");*/
		}
		x=sqrt(ion/(p-ion));
		f=2*PI*sqrt(ion/p)*(exp(-4*x*(PI/2-atan(x)))/(1-exp(-2*PI*x)));
		/* relativistic formula */
		tmp=sqrt(p*p+2*p);
		tmp2=(Z/p/ainv);
		sig=(4.*tmp2*tmp2*tmp2*tmp2*tmp2*ainv/14.5)*tmp*tmp*tmp*(
			4./3.+(p+1.)*(p-1.)/(p+2)*(
				1-log((p+1.+tmp)/(p+1.-tmp))/2./(p+1.)/tmp));
		/* with correction on atmosphere contents */
		prob += (z==7 ? 0.8 : 0.2)*sig*f;
	}
	/* Correction: include Argon photoeffect (needed at altitudes <90 km) */
	return prob*1.44;
}
#endif

/*
 * RandomComptonEnergy
 * -------------------
 * Energy of the scattered photon. Use simplified formula
 * from Pilkington and Anger [1971], corrected so that the
 * energy limits are correct [p/(1+2p),p].
 */
double RandomComptonEnergy(double p,double x)
{
	double s,p1;
	if(p<0 || x<0 || x>1) Error("bad input for RandomComptonEnergy");
	s=p/(1+0.5625*p);
	p1=p/(1+s*x+(2*p-s)*x*x*x);
	if(p1>p || p1<p/(1+2*p)) Error("internal in RandomComptonEnergy");
	return p1;
}

/*
 * ComptonCosAngle
 * ---------------
 * Cosine of the angle of Compton scattering - very simple formula.
 */
double ComptonCosAngle(double p, double p1)
{
	double cost;
	cost=1+1/p-1/p1;
	if(cost>1 || cost<-1) Error("Invalid parameters for ComptonCosAngle");
	return cost;
}

/*
 * PairProb
 * --------
 * Pair production by photons: probability per unit time at NmConv
 * using formula 3D-3000 from Motz et al [1969, doi:10.1103/RevModPhys.41.581].
 */
double PairProb(double k)
{
	double prob,ainv,e,e1,tmp,e12,Z,sig,log2k;
	int z; /* =7 for nitrogen, 8 for oxygen */
	// TO DISABLE THIS:
	// return 0;
	if(k<=2) return 0;
	/* Motz et al, 3D-0000 */
	ainv=137.036; /* inverse of fine structure constant */
	prob=0;
	for(z=7;z<=8;z++){
		Z=(double)z;
		if(k<3){
			e=(k-2)/(k+2);
			e1=2.*e/(1+sqrt(1-e*e));
			tmp=(k-2)/k;
			e12=e1*e1;
			sig=Z*Z/14.5/ainv/3.*tmp*tmp*tmp*(
                    1+(1./2.)*e1+(23./40.)*e12+(11./60.)*e1*e12+
                    (29./960.)*e12*e12); /* sigma_atom/(2*pi*r0^2*Zm) */
		}else{
			log2k=log(2.*k);
			tmp=4./k/k;
			sig=28./9.*log2k-218./27.+tmp*
				((6.-PI*PI/3.)*log2k-7./2.+log2k*log2k*(2./3.*log2k-1.)+PI*PI/6.+2*1.2020569)
                    -tmp*tmp*(3./16.*log2k+1./8.)-
                    tmp*tmp*tmp*(29./(9.*256.)*log2k-77./(27.*512.));
			sig*=Z*Z/ainv/(2*PI)/14.5; /* sigma_atom/(2*pi*r0^2*Zm) */
		}
		prob+=2*sig*(z==7 ? 0.8 : 0.2);
	}
	return prob;
}
