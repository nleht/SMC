/*
 * File: crossec.h
 * ---------------
 */

#ifndef _crossec_h_
#define _crossec_h_

typedef enum {REGULAR,RIGHT_ANGLE,FORWARD} ElectronScatteringT;

double CollisionRate(double g,double Emin,bool simple);
double FdTot(double p);
double FdIon(double p, double Emin);
double Theta2Growth(double p);
double DiffusionMu(double t);
void MollerIonization(double *px1,double *py1,double *pz1,
	double *px2,double *py2,double *pz2,
	double Emin,ElectronScatteringT type,bool simple);
/* Photon cross-sections */
double ComptonProb(double p);
double PhotoProb(double p);
double RandomComptonEnergy(double p,double x);
double ComptonCosAngle(double p, double p1);
double PairProb(double k);

#endif /* _crossec_h_ */
