/*
 * File: simulation.h
 * ------------------
 */

#ifndef _simulation_h_
#define _simulation_h_

#include "fid.h"
#include "particle.h"
#include "world.h"

/* Incomplete type for the simulation */
typedef struct SimulationCDT *Simulation;
typedef struct SimIOManagerCDT *SimIOManager;
typedef struct ProbabilityCDT *Probability;

Simulation InitSimulation(void);
World GetWorld(Simulation s);
Probability InitProbability(void);
SimIOManager InitIOManager(bool uniform_world);
void RunSimulation(Simulation s, SimIOManager iom, FIDT fid);

#endif /* _simulation_h_ */
