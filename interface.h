/*
 * File: interface.h
 * -----------------
 * This file provides a simple ANSI-compatible interface of
 * a program performing calculations with the user input.
 *
 * The function "main" is defined in "interface.c".
 * It calls the function "Main" which is defined by the user.
 * The application produced using "interface" takes 0 or 1
 * command-line arguments. If 0 arguments are provided, an
 * interaction regime is assumed. User types in required
 * information on the console when prompted. When 1 argument
 * is provided, it is considered to be a file name, from which
 * information is read. This file contains prompts as well as
 * inputs. The interaction is provided by function "GetNumbers".
 * 
 * Example:
 *
 *  int Main(void)
 *  {
 *    double N;
 *    GetNumbers("Enter N:","f",&N);
 *    StartCalculations();
 * --- this is not obligatory, just to show when we can close the script file
 *    do_calculations(N);
 *    return 0;
 *  }
 *
 * This "interface" works on Macintosh (MW CodeWarrior compiler)
 * and UNIX.
 *
 * Required files: util.h, util.c (for "Open").
 */

#ifndef _interface_h_
#define _interface_h_

#include <stdarg.h>
int Main(void);
void GetNumbers(char *prompt,char *myFormat, ...);
int Printf(char *format, ...);
int VPrintf(char *format, va_list args);
void StartCalculations(void);


#endif /* _interface_h_ */
