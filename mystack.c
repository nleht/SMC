/*
 * File: stack.c
 * -------------
 * The stack implementation, useful in Monte Carlo programs
 * implementing the "stacked-particle" method, in which
 * the particles are considered one at a time.
 * Only the particle at the top of the stack is active.
 * When a particle is created, it is pushed on the stack
 * and becomes active. When the particle dies, in is popped
 * from the stack.
 *
 * Needs: "util.c", "util.h"
 *
 * History:
 *
 * 5/24/99
 * Created for use with project "monte"
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "interface.h"
#include "mystack.h"

#define _stack_error_check_ 1

/*
 * The StackCDT data type.
 * -----------------------
 * The stuff that gets pushed on it is copied to "array",
 * therefore protected from accidental modifications.
 */
struct StackCDT{
	size_t elsize;
	unsigned long limit; /* maximum possible number of elements */
	char *array; /* where the actual stuff is stored */
	unsigned long top; /* the top of the stack */
	void **entries; /* array of pointers to actual stuff */
};

/*
 * GetNumElements, GetLimitStack
 * -----------------------------
 */
unsigned long GetNumStack(Stack stack)
{
#if _stack_error_check_
	if(stack==NULL) Error("GetNumStack: no stack found");
#endif
	return stack->top;
}

unsigned long GetLimitStack(Stack stack)
{
#if _stack_error_check_
	if(stack==NULL) Error("GetLimitStack: no stack found");
#endif
	return stack->limit;
}

/*
 * NewStack
 * --------
 * Allocate space and initialize
 */
Stack NewStack(unsigned long num,size_t size)
{
	Stack tmp;
	unsigned long i;
	
	if(!(tmp=malloc(sizeof(struct StackCDT))))
		Error("NewStack: Not enough memory");
	tmp->elsize=size;
	tmp->limit=num;
	if(!(tmp->array=malloc(size*num)))
		Error("NewStack: Not enough memory");
	if(!(tmp->entries=malloc(sizeof(void *)*num)))
		Error("NewStack: Not enough memory");
	for(i=0;i<num;i++)
		tmp->entries[i]=(void *)(tmp->array + size*i);
	ClearStack(tmp);
	return tmp;
}

/*
 * ClearStack
 * ----------
 */
void ClearStack(Stack stack)
{
#if _stack_error_check_
	if(stack==NULL) Error("ClearStack: no stack found");
#endif
	stack->top=0;
}

/*
 * Push
 * ----
 */
void *Push(Stack stack, void *element)
{
#if _stack_error_check_
	if(stack==NULL) Error("Push: no stack found");
	if(element==NULL) Error("Push: null element");
#endif
	if(stack->top >= stack->limit) return NULL; /* stack overflow */
	memcpy(stack->entries[stack->top++],element,stack->elsize);
	return element;
}

/*
 * Pop
 * ---
 */
void *Pop(Stack stack)
{
#if _stack_error_check_
	if(stack==NULL) Error("Pop: no stack found");
#endif
	if(stack->top <= 0)
		return NULL; /* stack underflow */
    return (stack->entries[--(stack->top)]);
}

/*
 * TopElement
 * ----------
 * The same as "Pop" except the element stays in the stack.
 * Be careful with it because it gives access into the stack internals.
 * Don't change the element information returned by this function.
 */
void *TopElement(Stack stack)
{
#if _stack_error_check_
	if(stack==NULL) Error("TopElement: no stack found");
#endif
	if(stack->top <= 0)
		return NULL; /* stack underflow */
    return (stack->entries[stack->top-1]);
}

/*
 * StackContents
 * -------------
 * For debugging.
 */
void StackContents(Stack stack, void (*printfun)(void *))
{
	unsigned long i;
#if _stack_error_check_
	if(stack==NULL) Error("StackContents: no stack found");
#endif
	Printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
	Printf("limit=%ld, top=%ld\n",stack->limit,stack->top);
	Printf("Elements:\n");
	for(i=0;i<stack->top;i++){
		printfun(stack->entries[i]);
	}
	Printf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
}

/*
 * GetElementByIndex
 * -----------------
 * Access all elements of the stack simultaneously.
 * Useful for saving the stack information to the file.
 * Usage example:
 *     i=0; while((e=GetElementByIndex(stack,i++))!=NULL) DoSomething(e);
 */
void *GetElementByIndex(Stack stack,unsigned long index)
{
#if _stack_error_check_
	if(stack==NULL) Error("GetElementByIndex: no stack found");
#endif
	if(index >= stack->top) return NULL;
	return stack->entries[index];
}

/*
 * CopyStack
 * ---------
 * We do not use any private members in this definition.
 */
void CopyStack(Stack s1,Stack s2)
{
	void *e;
	int i=0;
	while((e=GetElementByIndex(s1,i++))!=NULL){
		if(!Push(s2,e))
			Error("CopyStack: cannot copy element %d",i);
	}
}

/*
 * ReadStack
 * ---------
 * Does allocate the stack now.
 * Each stack record (element) is read using the function readfunc,
 * which reads the element into preallocated storage "e" and
 * returns NULL if the element cannot be read.
 * Named by analogy with ReadMatrix from "util.c".
 */
Stack ReadStack(char *fileName,size_t elsize,void *(*readfun)(FILE *,void *))
{
	FILE *file;
	void *e;
	unsigned long i;
	Stack s;
	
	if(!(e=malloc(elsize)))
		Error("ReadStack: not enough memory");
	/* 1. Count the elements */
	file=Open(fileName,"r");
	i=0;
	while(readfun(file,e)){
		i++;
	}
	fclose(file);
	Printf("ReadStack: counted %lu elements\n",i);
	/* 2. Allocate the stack */
	s=NewStack(i,elsize);
	/* 3. Read the elements */
	file=Open(fileName,"r");
	while(readfun(file,e)){
		Push(s,e);
	}
	fclose(file);
	free(e);
	
	return s;
}
