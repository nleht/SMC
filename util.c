/*
 * File: util.c
 *
 * History:
 *
 * 5/24/99
 * Added the following functions:
 *   double ExpIntegralE1(double x);
 *   double *ReadArray(char *fileName,int *n);
 * Moved "GetNumbers" to "interface.c", where it logically belongs.
 * Got rid of "InitInput" and "CleanupInput".
 *
 * 11/14/98
 * Added ReadMatrix.
 *
 * 6/19/97
 * Added Allocate1all and Allocate2all, which can take any type of data.
 *
 * 11/4/97
 * Changed implementation of Allocate2, Allocate3, Free2, Free3.
 *
 * 10/22/97
 * Added Cross, Rotate and FindOrthogonalVector
 *
 * 10/21/97
 * In the function "Open", added directory recognition
 * for Macintosh and UNIX.
 *
 * 8/22/97
 * Added Cartesian gradient and divergence
 * 
 * 7/2/97
 * Added boolean type "bool"
 *
 * 5/8/97
 * Added CleanupInput.
 *
 * 5/6/97
 * Added GetNumbers from another file and wrote InitInput.
 * Added subroutine "GetNumbers" (copied with some modifications
 * from another project).
 * Added comments.
 * Changed name of Interpolate1 and Interpolate0 to
 * Interpolate2 and Interpolate1.
 *
 * ?/?/96
 * Created for use with project "sp22".
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "util.h"
#include <time.h>

/******************************************************************
 *                    INPUT-OUTPUT FUNCTIONS                      *
 ******************************************************************/

/*
 * Open
 * ----
 * Open a file without hassle of checking for errors.
 * Can contain a relative path in UNIX format, e.g. "theDir/file.dat"
 */
FILE *Open(const char *fname,const char *flags)
{
	FILE *file;
	const char *relativePath;
#ifdef __MWERKS__
	int hierarchy;
	char MacPath[256],*Mname;
	char const *Uname;

	/* Parse the file name for the Macintosh */
	hierarchy=0;
	MacPath[0]=':';
	Mname=MacPath+1;
	for(Uname=fname;*Uname!='\0';Uname++){
		if(*Uname=='/'){
			*(Mname++) =':';
			hierarchy++;
		}else if(*Uname==':')
			Error("Sorry, ':' is not allowed in filenames");
		else
			*(Mname++)=*Uname;
	}
	*Mname='\0';
	if(hierarchy==0)
		relativePath=MacPath+1;
	else
		relativePath=MacPath;
#else /* UNIX */
	relativePath=fname;
#endif
	/* printf("Opening file '%s'\n",relativePath); */
	if(!(file=fopen(relativePath,flags)))
		Error("cannot open file '%s'",relativePath);  
	return file;
}

/*
 * ReadMatrix
 * ----------
 * Given the file name, read a 2d array, skipping blank lines
 * and determining its dimensions (m x n)
 */
double **ReadMatrix(char *fileName,int *m,int *n)
{
	FILE *file;
	int i,j,numRows,numCols,rows,cols;
	static char number[100];
	double **result;
	double dumb;
	char c;
	bool blank=FALSE;
	
	/* 1. Count the number of columns and rows */
	file=Open(fileName,"r");
	rows=0; cols=0; numRows=0; numCols=0;
	while((c=getc(file)) != EOF){
		/* read a line */
		cols=0;
		while(TRUE){
			/* skip blanks */
			while(c == ' ' || c == '\t') c=getc(file);
			/* check for the end of the line */
			if(c == '\n' || c == EOF){
				if(cols==0){/* skip a blank line */
					blank=TRUE;
					break;
				}else
					blank=FALSE;
				if(rows==0){
					/* count numbers in the first line */
					numCols=cols;
				}else{
					if(cols!=numCols)
						Error("Not all rows are of the same length in %s",fileName);
				}
				break;
			}
			/* read a number */
			i=0;
			while(c!=' ' && c!='\t' && c!='\n' && c!=EOF && i<80){
				number[i++]=c;
				c=getc(file);
			}
			number[i]='\0';
			if(sscanf(number,"%lf",&dumb)!=1)
				Error("%s is not a number",number);
			/* debug */ /* printf("row %d, column %d: read %f\n",rows,cols,dumb); getchar(); */
			cols++;
			/* end read a number */
		}
		if(!blank) rows++;
		if(c == EOF) break; /* end of the file */
	}
	numRows=rows;
	/* debug */ /* printf("numRows=%d, numCols=%d\n",numRows,numCols); */
	if(numRows==0) Error("file %s contains no data",fileName);
	rewind(file);
	
	/* 2. Read the contents of the file into an array */
	result=Allocate2(numRows,numCols);
	for(i=0;i<numRows;i++){
		for(j=0;j<numCols;j++){
			fscanf(file,"%lf",&(result[i][j]));
			/* fscanf(file,"%lf",&dumb);
			printf("array[%d][%d]==%f\n",i,j,dumb);*/
		}
	}
	fclose(file);
	*m=numRows; *n=numCols;
	return(result);
}

/*
 * ReadArray
 * ---------
 * Read one-dimensional array (row or column)
 */
double *ReadArray(char *fileName,int *n)
{
	double **tmp,*result=NULL;
	int i,m=0,n1,n2;
	
	tmp=ReadMatrix(fileName,&n1,&n2);
	if(n1==1){
		m=n2;
		result=Allocate1(m);
		for(i=0;i<m;i++) result[i]=tmp[0][i];
	}else if(n2==1){
		m=n1;
		result=Allocate1(m);
		for(i=0;i<m;i++) result[i]=tmp[i][0];
	}else{
		Error("file \"%s\" contains a 2D matrix",fileName);
	}	
	Free2(tmp,n1,n2);
	*n=m;
	return(result);
}

/*
 * SaveColumn
 * ----------
 * Save a 1D double array in a file with given file name.
 */
void SaveColumn(char *fileName,double *array,int n)
{
	FILE *file;
	int i;
	
	file=Open(fileName,"w");
	for(i=0;i<n;i++) fprintf(file,"%g\n",array[i]);
	fclose(file);
}

/*
 * SaveMatrix
 * ----------
 * Save a 2D double array in a file with given file name.
 */
void SaveMatrix(char *fileName,double **matrix,int m,int n)
{
	FILE *file;
	int i,j;
	
	file=Open(fileName,"w");
	for(i=0;i<m;i++){
		for(j=0;j<n;j++){
			fprintf(file,"\t%g",matrix[i][j]);
		}
		fprintf(file,"\n");
	}
	fclose(file);
}

/*
 * Error
 * -----
 * Print message and exit
 */
void Error(char *format, ...)
{
	va_list args;
	va_start(args,format);
	printf("\nError: ");
	vprintf(format,args);
	printf("\n");
	va_end(args);
	exit(1);
}

/******************************************************************
 *                    INTERPOLATION FUNCTIONS                     *
 ******************************************************************/

/*
 * TableGen
 * --------
 * Fill a table with interpolated values.
 * xi,yi - given data arrays of length ni,
 * x - given array of length n
 * y - array of y data of length n that is calculated by this function.
 */
void TableGen(double *x,double *y,int n,double *xi,double *yi,int ni)
{
  int i,lo,hi,med;
  double dx;

  if(ni<2) Error("invalid argument to TableGen: ni=%d",ni);
  for(i=0;i<n;i++){
    lo=0;
    hi=ni-1;
    while(hi>lo+1){
      med=(lo+hi)/2;
      dx=x[i]-xi[med];
      if(dx<0) hi=med;
      if(dx>=0) lo=med;
    }
    y[i]=yi[lo]+(yi[hi]-yi[lo])*(x[i]-xi[lo])/(xi[hi]-xi[lo]);
  }
}

/*
 * Interpolate2
 * ------------
 * Two-dimensional bilinear interpolation of array p of size (m x n)
 * at point (x,y) such that zero index corresponds to zero coordinate,
 * and dx, dy are coordinate increments corresponding to index
 * increment by 1.
 */
double Interpolate2(double **p,int m,int n,double dx,double dy,
                   double x,double y)
{
  int xr,yr;
  double delx,dely,val;
  
  delx=x/dx;
  xr=(int)floor(delx);
  if(xr >= m-1) xr=m-2;
  if(xr < 0) xr=0;
  delx-=xr;

  dely=y/dy;
  yr=(int)floor(dely);
  if(yr >= n-1) yr=n-2;
  if(yr < 0) yr=0;
  dely-=yr;
  
  val=p[xr][yr]*(1-delx)*(1-dely) + p[xr][yr+1]*(1-delx)*dely+
    p[xr+1][yr]*delx*(1-dely) + p[xr+1][yr+1]*delx*dely;
  return val;
}

/*
 * Interpolate1
 * ------------
 * Same but one-dimensional and linear interpolation instead
 * of bilinear
 */
double Interpolate1(double *p,int n, double dy, double y)
{
  int yr;
  double dely,val;
  dely=y/dy;
  yr=(int)floor(dely);
  if(yr >= n-1) yr=n-2;
  if(yr < 0) yr=0;
  dely-=yr;
  val=p[yr]*(1-dely) + p[yr+1]*dely;
  return val;
}

/*
 * Round
 * -----
 */
double Round(double **p,int m,int n,double dx,double dy,
                   double x,double y)
{
  int xr,yr;
  xr=(int)floor(x/dx+.5);
  if(xr > m-1) xr=m-1;
  if(xr < 0) xr=0;
  yr=(int)floor(y/dy+.5);
  if(yr > n-1) yr=n-1;
  if(yr < 0) yr=0;
  return p[xr][yr];
}

/******************************************************************
 *                     ALLOCATION FUNCTIONS                       *
 ******************************************************************/

/*
 * Allocate1
 * ---------
 * Allocate a one-dimensional array of doubles without
 * hassle of error-checking
 */
double *Allocate1(int n)
{
  double *tmp;
  if(!(tmp=malloc(n*sizeof(double))))
    Error("not enough memory");
  return tmp;
}

int *Allocate1Int(int n)
{
  int *tmp;
  if(!(tmp=malloc(n*sizeof(int))))
    Error("not enough memory");
  return tmp;
}

void *Allocate1all(int n,size_t size)
{
  void *tmp;
  if(!(tmp=malloc(n*size)))
    Error("not enough memory");
  return tmp;
}

/*
 * Allocate2
 * ---------
 * Same but 2-dimensional array
 */
double **Allocate2(int n1,int n2)
{
  double **tmp,*stor;
  int i;
  if(!(tmp=malloc(n1*sizeof(double*))))
    Error("not enough memory");
  if(!(stor=malloc(n1*n2*sizeof(double))))
    Error("not enough memory");
  for(i=0;i<n1;i++) tmp[i]=stor+i*n2;
  return tmp;
}

int **Allocate2Int(int n1,int n2)
{
  int **tmp,*stor;
  int i;
  if(!(tmp=malloc(n1*sizeof(int*))))
    Error("not enough memory");
  if(!(stor=malloc(n1*n2*sizeof(int))))
    Error("not enough memory");
  for(i=0;i<n1;i++) tmp[i]=stor+i*n2;
  return tmp;
}

void **Allocate2all(int n1,int n2,size_t size)
{
  void **tmp,*stor;
  int i;
  if(!(tmp=malloc(n1*size)))
    Error("not enough memory");
  if(!(stor=malloc(n1*n2*size)))
    Error("not enough memory");
  for(i=0;i<n1;i++) tmp[i]=(char *)stor+i*n2*size;
  return tmp;
}

/*
 * Allocate3
 * ---------
 * Same but 3-dimensional array
 */
double ***Allocate3(int n1,int n2,int n3)
{
  int i,j;
  double ***tmp,**stor1,*stor;
  if(!(tmp=malloc(n1*sizeof(double**))))
    Error("not enough memory");
  if(!(stor1=malloc(n1*n2*sizeof(double*))))
    Error("not enough memory");
  if(!(stor=malloc(n1*n2*n3*sizeof(double))))
    Error("not enough memory");
  for(i=0;i<n1;i++){
    tmp[i]=stor1+i*n2;
	for(j=0;j<n2;j++){
    	tmp[i][j]=stor+n3*(j+i*n2);
    }
  }
  return tmp;
}

/*
 * Free1
 * -----
 * Free a 1-d array. Not really needed.
 */
void Free1(double *arr,int n)
{
	free(arr);
}

/*
 * Free2
 * -----
 * Free a 2-d array
 */
void Free2(double **arr,int n1,int n2)
{
	free(*arr);
	free(arr);
}

/*
 * Free3
 * -----
 * Same but a 3-d array
 */
void Free3(double ***arr,int n1,int n2,int n3)
{
	free(**arr);
	free(*arr);
	free(arr);
}

/******************************************************************
 *                     VECTOR ANALYSIS FUNCTIONS                  *
 ******************************************************************/

/*
 * CylindricalGradient
 * -------------------
 * Take gradient of s in cylindrical coordinates,
 * and save it to dsdr,dsdz.
 * Cylindrical symmetry is assumed.
 */
void CylindricalGradient(double **s,double **dsdr,double **dsdz,
	      double dr,double dz,int m,register int n,
	      register int irbound,register int izbound)
{
  register int i,j,m1,n1;
  
  m1=m-1; n1=n-1;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      if(i==0)
	dsdr[i][j]=0;
      else if(i==m1){
	if(irbound==0)
	  /* no particular boundary conditions */
	  dsdr[i][j]=(s[i][j]-s[i-1][j])/dr;
	else if(irbound==1)
	  /* ds/dr==0 at r==rmax */
	  dsdr[i][j]=0;
	else if(irbound==2)
	  /* s==0 at r==rmax */
	  dsdr[i][j]=-s[i-1][j]/dr/2;
      }else{
	dsdr[i][j]=(s[i+1][j]-s[i-1][j])/dr/2;
      }

      if(j==0){
	if(izbound==0)
	  /* no particular boundary conditions */
	  dsdz[i][j]=(s[i][j+1]-s[i][j])/dz;
	else if(izbound==1)
	  /* ds/dz==0 at z=zmin */
	  dsdz[i][j]=0;
	else if(izbound==2)
	  /* s==0 at z==zmin */
	  dsdz[i][j]=s[i][j+1]/dz/2;
      }else if(j==n1){
	if(izbound==0)
	  /* no particular boundary conditions */
	  dsdz[i][j]=(s[i][j]-s[i][j-1])/dz;
	else if(izbound==1)
	  /* ds/dz==0 at z==zmax */
	  dsdz[i][j]=0;
	else if(izbound==2)
	  /* s==0 at z==zmax */
	  dsdz[i][j]=-s[i][j-1]/dz/2;
      }else
	dsdz[i][j]=(s[i][j+1]-s[i][j-1])/dz/2;
    }
  }
}

/*
 * CartesianGradient
 * -----------------
 * Same, but in Cartesian coordinates x,z
 */
void CartesianGradient(double **s,double **dsdx,double **dsdz,
	      double dx,double dz,int m,register int n,
	      register int ixbound,register int izbound)
{
	register int i,j,m1,n1;
  
	m1=m-1; n1=n-1;
	for(i=0;i<m;i++){
		for(j=0;j<n;j++){
			if(i==0){
				if(ixbound==0)
					/* no particular boundary conditions */
					dsdx[i][j]=(s[i+1][j]-s[i][j])/dx;
				else if(ixbound==1)
					/* ds/dx==0 at x=xmin */
					dsdx[i][j]=0;
				else if(ixbound==2)
					/* s==0 at x==xmin */
					dsdx[i][j]=s[i+1][j]/dx/2;
			}else if(i==m1){
				if(ixbound==0)
					/* no particular boundary conditions */
					dsdx[i][j]=(s[i][j]-s[i-1][j])/dx;
				else if(ixbound==1)
					/* ds/dx==0 at x==xmax */
					dsdx[i][j]=0;
				else if(ixbound==2)
					/* s==0 at x==xmax */
				dsdx[i][j]=-s[i-1][j]/dx/2;
			}else{
				dsdx[i][j]=(s[i+1][j]-s[i-1][j])/dx/2;
			}

      if(j==0){
	if(izbound==0)
	  /* no particular boundary conditions */
	  dsdz[i][j]=(s[i][j+1]-s[i][j])/dz;
	else if(izbound==1)
	  /* ds/dz==0 at z=zmin */
	  dsdz[i][j]=0;
	else if(izbound==2)
	  /* s==0 at z==zmin */
	  dsdz[i][j]=s[i][j+1]/dz/2;
      }else if(j==n1){
	if(izbound==0)
	  /* no particular boundary conditions */
	  dsdz[i][j]=(s[i][j]-s[i][j-1])/dz;
	else if(izbound==1)
	  /* ds/dz==0 at z==zmax */
	  dsdz[i][j]=0;
	else if(izbound==2)
	  /* s==0 at z==zmax */
	  dsdz[i][j]=-s[i][j-1]/dz/2;
      }else
	dsdz[i][j]=(s[i][j+1]-s[i][j-1])/dz/2;
    }
  }
}

/*
 * CylindricalDiv
 * --------------
 * Divergence of vector field (pr,pz) in cylindrical coordinates.
 * It is assumed that angular component is zero.
 */
void CylindricalDiv(double **pr,double **pz,double **divp,
	 double dr,double dz,int m,register int n)
{
  register int i,j,m1,n1;
  double dprdr,dpzdz;
  
  m1=m-1; n1=n-1;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      if(i==0)
	/* Here we are using Lopitals rule to resolve 0/0 */
	dprdr=pr[i+1][j]*2/dr;
      else if(i==m1)
	dprdr=(pr[i][j]*i-pr[i-1][j]*(i-1))/dr/(i-.5);
      else
	dprdr=(pr[i+1][j]*(i+1)-pr[i-1][j]*(i-1))/dr/2/i;

      if(j==0)
	dpzdz=(pz[i][j+1]-pz[i][j])/dz;
      else if(j==n1)
	dpzdz=(pz[i][j]-pz[i][j-1])/dz;
      else
	dpzdz=(pz[i][j+1]-pz[i][j-1])/dz/2;
            
      divp[i][j]=dprdr+dpzdz;
    }
  }
}

/*
 * CartesianDiv
 * ------------
 * Same, but in Cartesian coordinates x,z
 */
void CartesianDiv(double **px,double **pz,double **divp,
	 double dx,double dz,int m,register int n)
{
  register int i,j,m1,n1;
  double dpxdx,dpzdz;
  
  m1=m-1; n1=n-1;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      if(i==0)
	dpxdx=(px[i+1][j]-px[i][j])/dx;
      else if(i==m1)
	dpxdx=(px[i][j]-px[i-1][j])/dx;
      else
	dpxdx=(px[i+1][j]-px[i-1][j])/dx/2;

      if(j==0)
	dpzdz=(pz[i][j+1]-pz[i][j])/dz;
      else if(j==n1)
	dpzdz=(pz[i][j]-pz[i][j-1])/dz;
      else
	dpzdz=(pz[i][j+1]-pz[i][j-1])/dz/2;
            
      divp[i][j]=dpxdx+dpzdz;
    }
  }
}
 
/*
 * SmallRotate
 * -----------
 * Rotate vector a by a small vector dp. Or, by an angle 2 arctan(p/2).
 */
void SmallRotate(double *ax,double *ay,double *az,double dpx,double dpy,double dpz)
{
	double coef,x1,y1,z1,x2,y2,z2;
	dpx/=2; dpy/=2; dpz/=2;
	coef=2/(1 + dpx*dpx + dpy*dpy + dpz*dpz);
	Cross(dpx,dpy,dpz,*ax,*ay,*az,&x1,&y1,&z1);
	x1 += *ax;
	y1 += *ay;
	z1 += *az;
	Cross(dpx,dpy,dpz,x1,y1,z1,&x2,&y2,&z2);
	*ax += coef * x2;
	*ay += coef * y2;
	*az += coef * z2;
}

/*
 * Rotate
 * ------
 * Rotate vector a by an angle represented by vector dp.
 */
void Rotate(double *ax,double *ay,double *az,double dpx,double dpy,double dpz)
{
	double th,coef,x1,y1,z1,x2,y2,z2;
	/* needed angle */
	th=sqrt(dpx*dpx+dpy*dpy+dpz*dpz);
	/* printf("\nby %lf\n",th); */
	/* the vector which we can use for SmallRotate */
	if(cos(th/2)<1e-6){
		/* printf("\nby pi\n"); */
		/* rotation by pi, for which tan(th/2)=infinity. */
		Cross(dpx,dpy,dpz,*ax,*ay,*az,&x1,&y1,&z1);
		Cross(dpx,dpy,dpz,x1,y1,z1,&x2,&y2,&z2);
		coef=2./(th*th);
		*ax+=coef*x2;
		*ay+=coef*y2;
		*az+=coef*z2;
		return;
	}
	if(th>1e-6)
		coef=2*tan(th/2)/th;
	else
		coef=1;
	dpx*=coef; dpy*=coef; dpz*=coef;
	/* printf("small rot=%g, %g, %g\n",dpx,dpy,dpz); */
	SmallRotate(ax,ay,az,dpx,dpy,dpz);
}

/*
 * Cross
 * -----
 * Cross product of 2 vectors.
 */
void Cross(double ax,double ay,double az,
		double bx,double by,double bz,double *cx,double *cy,double *cz)
{
	*cx = ay * bz - az * by;
	*cy = az * bx - ax * bz;
	*cz = ax * by - ay * bx;
}

/*
 * FindOrthogonalVector
 * --------------------
 * Find a vector parpendicular to p of length dp in a random direction (dpx,dpy).
 * Component dpx is orthogonal to p and in the (x,y) plane.
 */
void FindOrthogonalVector(double px,double py,double pz,double dpx,double dpy,
		double *rx,double *ry,double *rz)
{
	double p,x1x,x1y,x1z,y1x,y1y,y1z,cost,pp;
	/* xhat,yhat are the basis for the plane orthogonal to e */
	p=sqrt(px*px+py*py+pz*pz);
	if(p==0) Error("Trying to find an orthogonal to a zero vector");
	cost=pz/p;
	if(cost-1>=-1e-7){
		x1x=1; x1y=0; x1z=0;
		y1x=0; y1y=1; y1z=0;
	}else if(cost+1<=1e-7){
		x1x=1; x1y=0; x1z=0;
		y1x=0; y1y=-1; y1z=0;
	}else{ /* large angle with z-axis */
		pp=sqrt(px*px+py*py);
		x1x = py / pp;
		x1y = - px / pp;
		x1z=0;
		Cross(px,py,pz,x1x,x1y,x1z,&y1x,&y1y,&y1z);
		y1x/=p;
		y1y/=p;
		y1z/=p;
	}
	*rx = dpx*x1x + dpy*y1x;
	*ry = dpx*x1y + dpy*y1y;
	*rz = dpy*y1z;
}

/*
 * ConvertToLabFrame
 * -----------------
 * Vector (x,y,z) is given in respect to an RF where p=(0,0,p). We should convert
 * it to the lab frame where p=(px,py,pz), conserving the angles.
 */
void ConvertToLabFrame1(double px,double py,double pz,
		double *x,double *y,double *z)
{
/* The same task is accomplished using vector rotation */
	double coef;
	coef=2./(sqrt(px*px+py*py+pz*pz)+pz);
	SmallRotate(x,y,z,-coef*py,coef*px,0.);
}

void ConvertToLabFrame(double px,double py,double pz,
		double *x,double *y,double *z)
{
	double pp,p,cost,x1,y1,z1;
	/* Conversion to a laboratory RF */
	x1=*x; y1=*y; z1=*z;
	pp=sqrt(px*px+py*py);
	p=sqrt(px*px+py*py+pz*pz);
	cost=pz/p;
	if(cost+1<=1e-7){ /* use inverted RF */
		*x=-x1;
		*y=-y1;
		*z=-z1;
	}else if(cost-1 < -1e-7){ /* rotate the RF */
		/* printf("rotating {%lf, %lf, %lf} in base {%lf, %lf, %lf}\n",
			*x,*y,*z,px,py,pz); */
		*x=cost*px/pp*x1 - py/pp*y1 + px/p*z1;
		*y=cost*py/pp*x1 + px/pp*y1 + py/p*z1;
		*z=-pp/p*x1 + cost*z1;
	}
}

/*
 * ConvertToParticleFrame
 * ----------------------
 * An inverse operation: the vector (x,y,z) is now in the lab frame
 * and has to be converted to the laporatory frame where p=(px,py,pz).
 */
void ConvertToParticleFrame1(double px,double py,double pz,
		double *x,double *y,double *z)
{
/* The same task is accomplished using vector rotation */
	double coef;
	coef=2./(sqrt(px*px+py*py+pz*pz)+pz);
	SmallRotate(x,y,z,coef*py,-coef*px,0.);
}

void ConvertToParticleFrame(double px,double py,double pz,
		double *x,double *y,double *z)
{
	double pp,p,cost,x1,y1,z1;
	/* Conversion to a laboratory RF */
	x1=*x; y1=*y; z1=*z;
	pp=sqrt(px*px+py*py);
	p=sqrt(px*px+py*py+pz*pz);
	cost=pz/p;
	if(cost+1<=1e-7){ /* use inverted RF */
		*x=-x1;
		*y=-y1;
		*z=-z1;
	}else if(cost-1 < -1e-7){ /* rotate the RF */
		/* printf("rotating {%lf, %lf, %lf} in base {%lf, %lf, %lf}\n",
			*x,*y,*z,px,py,pz); */
		*x=((pz/pp)*(px*x1+py*y1)-pp*z1)/p;
		*y=(-py*x1+px*y1)/pp;
		*z=(px*x1+py*y1+pz*z1)/p;
	}
}

/******************************************************************
 *                 VARIOUS MATHEMATICAL FUNCTIONS                 *
 ******************************************************************/
/*
 * ExpIntegralE1
 * -------------
 * Exponential integral E1(x)=\int_x^\infty exp(-t)/t dt
 * The algorithm is from Abramowitz and Stegun, page 231.
 */
double ExpIntegralE1(double x)
{
	if(x<=0.) Error("ExpIntegralE1: x<=0");
	if(x<1.)
		return -log(x) - 0.57721566490153286  + x*0.99999193
				- x*x*0.24991055 + x*x*x*0.05519968
				- x*x*x*x*0.00976004+x*x*x*x*x*0.00107857;
	else
		return exp(-x)/x*
			(x*x + x* 2.334733 + 0.250621)/
			(x*x + x*3.330657 + 1.681534);
}

/*
 * ExpIntegralE1Inv
 * ----------------
 * Inverse of ExpIntegralE1
 */
double ExpIntegralE1Inv(double y)
{
	double t,tnew,geuler,err,ey1,ly2,fp,emin=1e-7;
	int c;
	
	if(y<0) Error("ExpIntegralE1Inv: y=%lf<0",y);
	if(y>0.1){ /* y>0.1 -- small x */
		geuler=0.57721566490153286;
		ey1=exp(-y-geuler);
		t=ey1;
		err=1; c=0;
		while(err>emin){
			fp=ey1*exp(-t)/t;
			tnew=t+(ey1-exp(-ExpIntegralE1(t)-geuler))/fp;
			err=fabs(tnew-t);
			//printf("err=%lg\n",tnew-t);
			t=tnew;
			c=c+1;
			if(c>100) Error("ExpIntegralE1Inv: Too many iterations");
		}
	}else{ /* y<=0.1 -- large x */
		ly2=log(y);
		t=1;
		err=1; c=0;
		while(err>emin){
			fp=-exp(-t)/t/ExpIntegralE1(t);
			tnew=t+(ly2-log(ExpIntegralE1(t)))/fp;
			err=fabs(tnew-t);
			t=tnew;
			c=c+1;
			if(c>100) Error("ExpIntegralE1Inv: Too many iterations");
		}
	}
	return t;
}





/* THESE FUNCTIONS WERE REPLACED (SEE BELOW) */
#if 0

/*
 * InitMyRandom
 * ------------
 * Initialize random calculations.
 */
void InitMyRandom(int rseed)
{
#ifndef unix
	srand(rseed);
#else /* UNIX */
	srand48(rseed);
#endif
}

/*
 * MyRandom
 * --------
 * Generates a random floating-point number in the interval
 * from 0 to 1.
 */
double MyRandom(void)
{
#ifndef unix
	#error UNIX
	double base;
	/* Simulate 48 - binary random number */
	base=(double)RAND_MAX+1;
	return ((rand()/base+rand())/base+rand())/base;*/
	/* return (double)rand()/RAND_MAX; */
#else  /* UNIX */
	return drand48();
#endif
}

#endif /* "0" */
/* END OF THE REPLACED FUNCTIONS */




static unsigned int seed;
static int count624;
static unsigned int mt[624];

void InitMyRandom(int rseed)
{
#ifndef unix
	srand(rseed);
#else /* UNIX */
    /* srand48(rseed); */
    time_t curtime;
    if (rseed<0){
    	time(&curtime);      // Get current time in seed.
    	seed = (unsigned int)curtime;
    }else{
    	seed=(unsigned int)rseed;
    }

    count624 = 624;
    int i,j;

    mt[0] = seed;
    j = 1;
    // use multipliers from  Knuth's "Art of Computer Programming" Vol. 2, 3rd Ed. p.106
    for(i=j; i<624; i++) {
        mt[i] = (1812433253 * ( mt[i-1]  ^ ( mt[i-1] >> 30)) + i);
    }
#endif
}

 /* MyRandom
 * --------
 * Generates a random floating-point number in the interval
 * from 0 to 1.
 * - now uses root's mersenne twistor method [BEC]
 *   straight copied from root, up to conversion from c++ to c.
 */
double MyRandom(void)
{
#ifndef unix
	#error UNIX
	double base;
	/* Simulate 48 - binary random number */
	base=(double)RAND_MAX+1;
	return ((rand()/base+rand())/base+rand())/base;
	/* return (double)rand()/RAND_MAX; */
#else  /* UNIX */
	/* return drand48(); */
    unsigned int y;

    const int  kM = 397;
    const int  kN = 624;
    const unsigned int kTemperingMaskB =  0x9d2c5680;
    const unsigned int kTemperingMaskC =  0xefc60000;
    const unsigned int kUpperMask =       0x80000000;
    const unsigned int kLowerMask =       0x7fffffff;
    const unsigned int kMatrixA =         0x9908b0df;

    if (count624 >= kN) {
        register int i;

        for (i=0; i < kN-kM; i++) {
            y = (mt[i] & kUpperMask) | (mt[i+1] & kLowerMask);
            mt[i] = mt[i+kM] ^ (y >> 1) ^ ((y & 0x1) ? kMatrixA : 0x0);
        }

        for (   ; i < kN-1    ; i++) {
            y = (mt[i] & kUpperMask) | (mt[i+1] & kLowerMask);
            mt[i] = mt[i+kM-kN] ^ (y >> 1) ^ ((y & 0x1) ? kMatrixA : 0x0);
        }

        y = (mt[kN-1] & kUpperMask) | (mt[0] & kLowerMask);
        mt[kN-1] = mt[kM-1] ^ (y >> 1) ^ ((y & 0x1) ? kMatrixA : 0x0);
        count624 = 0;
    }

    y = mt[count624++];
    y ^=  (y >> 11);
    y ^= ((y << 7 ) & kTemperingMaskB );
    y ^= ((y << 15) & kTemperingMaskC );
    y ^=  (y >> 18);

    if (y) return ( (double) y * 2.3283064365386963e-10); // * Power(2,-32)
    return MyRandom();
#endif
}
