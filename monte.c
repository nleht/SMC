/*
 * File: monte.c
 * -------------
 * UNITS USED IN THIS PROGRAM: 
 * Length    -- 1 km.
 * Nm        -- nconv = 4.6e18 m^{-3}
 *  (for uniform calculations, Nm=1)
 * Time      -- 1 sec
 * E,B field -- inverse second.
 * v         -- speed of light c
 * p         -- mc, where m is electron mass
 * Energy    -- mc^2.
 */

#include <stdio.h>
#include <math.h>
#include "fid.h"
#include "interface.h"
#include "util.h"
#include "simulation.h"
#include "wait.h"
#include "world.h"

int Main(void);

int Main(void)
{
	Simulation s;
	SimIOManager iom;
	FIDT fid; // initial distribution
	int seed;
	long cycle,linelen;
	
	GetNumbers("The waiting cycle and line length?","ll",&cycle,&linelen);
	InitWait(cycle,linelen);
	GetNumbers("Random number generator seed (0<=s<32000), -1 for time-based:","d",&seed);
	InitMyRandom(seed);
	fid = InitFID();
	s = InitSimulation();
	iom = InitIOManager(IsUniform(GetWorld(s)));
	RunSimulation(s,iom,fid);
	return 0;
}
